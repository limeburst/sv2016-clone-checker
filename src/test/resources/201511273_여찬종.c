#include<stdio.h>
 #include<stdlib.h>
 #include<ctype.h>
 #include <string.h>

 #define SIZE 20

 typedef struct ListNode{
         char word[SIZE];
         struct ListNode *link;
 }ListNode;

 

 ListNode *create_node(char *word,ListNode *link){
         ListNode *new_node;
         new_node=(ListNode *)malloc(sizeof(ListNode));
         if( new_node == NULL )
                 error("memory error");
         strcpy(new_node->word,word);
         new_node->link=link;
         return (new_node);
 }

 void insert(ListNode **phead,ListNode **other,ListNode *node,int *num){
         ListNode *p,*q;
         q=node;
         if( *phead == NULL ){
                 *phead=node;
                 node->link=node;
                 (*num)++;
         }
         else{
                 p=*phead;
                 do{
                         if(strcmp(p->word,q->word)==0){
                                 return;
                         }
                         p=p->link;
                 }while(p!=*phead);
         }
         p=*other;
         if( *other != NULL){
                 do{
                         if(strcmp(p->word,q->word)==0){
                                 return;
                         }
                         p=p->link;
                 }while(p!=*other);
         }
         
         if( *phead != NULL ){
                 node->link=(*phead)->link;
                 (*phead)->link=node;
                 (*num)++;
         }
 }


 void display(ListNode *head,int k){
         ListNode *p;
         if( head==NULL )
                 return ;
         p=head;
         printf("%c\n",(char)k+65);
         do{
                 printf("%s ",p->word);
                 p=p->link;
         }while(p!=head);
         printf("\n\n");
 }

 void search(ListNode *head,ListNode *other,char *searchWord){ 
         ListNode *p;

         p=head;
         do{
                 if(strcmp(p->word,searchWord)==0){
                         printf("단어장에 있습니다.\n");
                         return;
                 }
                 p=p->link;
         }while(p!=head);
         p=other;
         do{
                 if(strcmp(p->word,searchWord)==0){
                         printf("단어장에 있습니다.\n");
                         return;
                 }
                 p=p->link;
         }while(p!=other);
         printf("단어장에 없습니다.\n");
         return;
 }


 void error(char *message){
         fprintf(stderr,"%s\n",message);
         exit(1);
 }


 int main(){
         ListNode *alphabet[SIZE];
         ListNode *theOthers=NULL;

         int choice;
         int i,j,k;
         int num[SIZE];
         char ch;
         char str[SIZE];
         char searchWord[SIZE];

         FILE *file=fopen("sample1.txt","r");
     FILE *file1=fopen("sample11.txt","w");
     if(file==NULL || file1==NULL){
                 printf("file open error!\n");
         return 1;
     }

         while(!feof(file)){
                 ch=fgetc(file);
         if(isalpha(ch)==0)
                         ch=' ';
                 else
                         ch=toupper(ch);
         fputc(ch,file1);
         }

         fclose(file);
         fclose(file1);

         file1=fopen("sample1.txt","r");
         if(file1==NULL){
                 printf("file1 open error!\n");
                 return 1;
         }

         for(i=0;i<26;i++){
                 alphabet[i]=NULL;
         }
         for(i=0;i<26;i++){
                 num[i]=1;
         }
         
         while(!feof(file1)){
                 fscanf(file1,"%s",str);
                 k=(int)str[0];
                 if(num[k-65]<=6)
                         insert(&alphabet[k-65],&theOthers,create_node(str,NULL),&num[k-65]);
                 else
                         insert(&theOthers,&alphabet[k-65],create_node(str,NULL),&num[k-65]);
         }

         fclose(file1);
         
         for(i=0;i<26;i++){
                 display(alphabet[i],i);
         }

         display(theOthers,100);

         while(1){
                 
                 printf("기능을 선택하세요       \n");
                 printf("[1] Search   [2] Insert \n");
                 printf("[3] Delete   [4] Quit   \n");
                 
                 printf(" :");
                 scanf("%d",&choice);
         
                 switch(choice){
                         case 1:
                                 printf("찾을 단어를 입력하세요.: ");
                                 scanf("%s",searchWord);
                                 for(j=0;j<strlen(searchWord);j++)
                                         searchWord[j]=toupper(searchWord[j]);
                                 k=(int)searchWord[0];
                                 search(alphabet[k-65],theOthers,searchWord);
                                 break;
                         case 2:
                                 printf("추가할 단어를 입력하세요.: ");
                                 scanf("%s",searchWord);
                                 for(j=0;j<strlen(searchWord);j++)
                                         searchWord[j]=toupper(searchWord[j]);
                                 k=(int)searchWord[0];
                                 if(num[k-65]<=5)
                                         insert(&alphabet[k-65],&theOthers,create_node(searchWord,NULL),&num[k-65]);
                                 else
                                         insert(&theOthers,&alphabet[k-65],create_node(searchWord,NULL),&num[k-65]);
                                 break;
                 }
         }
 }
