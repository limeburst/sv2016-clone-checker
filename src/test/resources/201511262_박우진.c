#include <stdio.h>
#include <stdlib.h>
#include <String.h>
#include <ctype.h>
#define size 27
#define MAX 1000

typedef struct Node{
	struct Node *prevNode;
	struct Node *nextNode;
	char data[256];
}node;
typedef struct LinkedList{
	node *header;
	node *trailer;
}List;
void init(List *list){
	for(int i=0;i<size;i++){
		(list+i)->header=(node *)malloc(sizeof(node));
		(list+i)->trailer=(node *)malloc(sizeof(node));
		(list+i)->header->prevNode=NULL;
		(list+i)->header->nextNode=(list+i)->trailer;
		(list+i)->trailer->prevNode=(list+i)->header;
		(list+i)->trailer->nextNode=NULL;
	}
}
void Insert(List *list,char data[]){
	node *newNode = (node *)malloc(sizeof(node));
	newNode->prevNode=list->trailer->prevNode;
	newNode->nextNode=list->trailer;
	strcpy(newNode->data,data);
	list->trailer->prevNode->nextNode=newNode;
	list->trailer->prevNode=newNode;
}

void selectMenu(){
	printf("□□□□□□□□□□□□□□□□□\n");
	printf("□               □\n");
	printf("□  [1]Search    □\n");
	printf("□  [2]Insert    □\n");
	printf("□  [3]Delete    □\n");
	printf("□  [4] QUIT	□\n");
	printf("□               □\n");
	printf("□□□□□□□□□□□□□□□□□\n");
}
int getf_word(char * alpha){
	return tolower(alpha[0])-97;
}
node* Search(List *list, char input_data[]){
	int index =input_data[0]-97;
	node *temp=(list+index)->header->nextNode;
	while(temp != (list+index)->trailer){
		if(strcmp(input_data,temp->data)==0){
			printf("단어장에 있습니다.\n");
			break;
		}
		temp = temp->nextNode;
	}
	return temp;
}
void Delete(List *list, char input_data[]){
        node *temp = Search(list, input_data);
	temp -> nextNode -> prevNode = temp -> prevNode;
	temp -> prevNode -> nextNode = temp -> nextNode;
	printf("삭제되었습니다.\n");
	free(temp);
}
void loadfile(List *list){
	char line[1024];
	char word[256];
	char *temp;
	char m_data[256];
	FILE *f;
	f = fopen("sample4.txt","r");	
		while(fgets(line, 1024, f)){
			temp = strtok(line," \n");
			while(temp != NULL){
				getf_word(temp);
				Insert(&list[getf_word(temp)],temp);
				temp = strtok(NULL," \n");
			}
			break;

		}
}
void saveFile_b(List *list){
	FILE *f;
	node *temp=(node*)malloc(sizeof(node));
	temp = list->header;
	f = fopen("voc.txt","ab+");
	while(temp !=NULL){
		fwrite(temp,sizeof(temp),1,f);
		temp = temp -> nextNode;
	}
	fclose(f);
	//읽어올때 맨앞에 한단어만 읽어오고 있고 있다, 저장할때 다음으로 넘어가면서 저장해야됨. 따라서 읽을때 node의 맨처음부터 끝까지 읽어서 있는지 없는지 판별.
}
void saveFile_a(List *list){
	FILE *f;
	node *temp = (node*)malloc(sizeof(node));
	temp = list -> header;
	f = fopen("voc.bin","a+");
	while(temp != NULL){
		fprintf(f,"%s",temp->data);
		temp = temp ->nextNode;
	}
	fclose(f);
}
void loadfile_b(List *list){
	FILE *f;
	f= fopen("voc.bin","a+");
}

int main(){
	int s_num;
	int num;
	char m_data[256];
	List *list = (List *)malloc(sizeof(List)*size);
	init(list);
	while(1){
		selectMenu();
		printf("원하는 메뉴를 선택하세요.\n");
		scanf("%d",&s_num);
		if(s_num==1){
			printf("찾을 단어를 입력하세요\n");
			scanf("%s",m_data);
			saveFile_b(list);
			Search(list,m_data);
		}
		else if(s_num == 2){
			printf("저장할 유형을 고르시오 1.ASCII 2.BINARY\n");
			scanf("%d",&num);
				if(num==1){
					printf("추가할 단어를 입력하세요\n");
					scanf("%s",m_data);
					Insert(list, m_data);
					saveFile_a(list);
				}
				else if(num == 2){
					printf("추가할 단어를 입력하세요\n");
					scanf("%s",m_data);
					Insert(list, m_data);
					saveFile_b(list);
				}
		}
		else if(s_num ==3){
			loadfile_b(list);
			printf("삭제할 단어를 입력하세요.\n");
			scanf("%s",m_data);
			Delete(list, m_data);
		}
		else if(s_num ==4){
			printf("종료되었습니다\n");
			break;
		}
	}
}
