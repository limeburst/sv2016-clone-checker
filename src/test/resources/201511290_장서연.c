#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>


struct NODE
{
	char data[20];
	struct NODE *link;
};

struct LIST
{
	struct NODE *header;
};

void add(struct LIST *list,char word[])
{
	struct NODE *temp=(struct NODE*)malloc(sizeof(struct NODE));
	temp->link=list->header;
	list->header=temp;
	strcpy(temp->data,word);
}

int rem(struct LIST *list[],char word[])
{
	int i=0;
	for(i=0;i<26;i++)
	{
		struct NODE *temp=list[i]->header;
		struct NODE *prev=NULL;
		while(temp!=NULL)
		{
			if(strcmp(temp->data,word)==0)
			{
				if(prev==NULL)
				{
					list[i]->header=temp->link;
					free(temp);
					temp=list[i]->header;
				}
				else if(prev!=NULL)
				{
					prev->link=temp->link;
					free(temp);
					temp=prev->link;
				}
				return i;
			}
			else
			{
				prev=temp;
				temp=temp->link;
			}
		}
	}
	return i;
}

int find(struct LIST *list[],char word[])
{
	int i=0;
	for(i=0;i<26;i++)
	{
		struct NODE *temp=list[i]->header;
		while(temp!=NULL)
		{
			if(strcmp(temp->data,word)==0)
			{
				return 1;
			}
			temp=temp->link;
		}
	}
	return 0;
}

int main(void)
{
	FILE *fp;
	FILE *f;
	char buffer[512];
	char *word;
	char *p;
	int i=0;
	int j=0;
	int number=0;
	struct LIST *array[26];
	struct LIST *voc[26];
	char search[20];
	struct NODE *t=NULL;
	int control=0;
	struct NODE *del;
	struct NODE *tem;
	for(i=0;i<26;i++)
	{
		array[i]=(struct LIST*)malloc(sizeof(struct LIST));
		array[i]->header=NULL;
	}
	i=0;
	for(i=0;i<26;i++)
	{
		voc[i]=(struct LIST*)malloc(sizeof(struct LIST));
		voc[i]->header=NULL;
	}
	fp= fopen("sample.txt", "r");
	while(!feof(fp))
	{
		fgets(buffer,sizeof(buffer),fp);
		word=strtok(buffer," ");
		while(word!=NULL)
		{
			char temp[20];
			char sub[20];
			strcpy(temp,word);
			i=0;
			while(!isalpha(temp[i]))
			{
				i++;
			}
			j=strlen(temp)-1;
			while(!isalpha(temp[j]))
			{
				temp[j]='\0';
				j--;
			}
			if(i<strlen(word))
			{
				strcpy(sub,temp+i);
				if(find(array,sub)==0)
				{
					if(sub[0]>=65&&sub[0]<=90)
					{
							add(array[sub[0]-65],sub);
					}
					else if(sub[0]>=97&&sub[0]<=122)
					{	
							add(array[sub[0]-97],sub);
					}
					else NULL;
				}
			}
			word=strtok(NULL," ");
		}
	}
	fclose(fp);

	f=fopen("voc.txt","rb");
	if(f!=NULL)
	{
		while(!feof(f))
		{
			fgets(buffer,sizeof(buffer),f);
			word=strtok(buffer,"\n\r");
			while(word!=NULL)
			{
				char temp[20];
				strcpy(temp,word);
				if(find(voc,temp)==0)
				{
					if(temp[0]>=65&&temp[0]<=90)
					{
						add(voc[temp[0]-65],temp);
					}
					else if(temp[0]>=97&&temp[0]<=122)
					{	
						add(voc[temp[0]-97],temp);
					}
					else NULL;
				}
				word=strtok(NULL," ");
			}
		}
	}
	fclose(f);

	while(1)
	{
		printf("기능을 선택하세요: \n[1]Search [2]Insert\n[3]Delete [4]Search in voca\n[5]store in ASCII [6]store in binary\n[7]Quit\n");
		scanf("%d",&control);

		if(control==1)
		{
			printf("사전에서 찾을 단어를 입력하세요:\n");
			scanf("%s",search);
			if(find(array,search)==1)
			{
	 			printf("->단어장에 있습니다.\n");
	 			if(find(voc,search)==0)
	 			{
	 				if(search[0]>=65&&search[0]<=90)
					{
					add(voc[search[0]-65],search);
					}
					else if(search[0]>=97&&search[0]<=122)
					{	
						add(voc[search[0]-97],search);
					}
				}
				else NULL;
			}
			else
				printf("->단어장에 없습니다.\n");
		}

		if(control==2)
		{
			printf("입력할 단어를 입력하세요:\n");
			scanf("%s",search);
			if(find(voc,search)==1)
			{
	 			printf("->중복된 단어입니다.\n");
			}
			else
			{
				if(search[0]>=65&&search[0]<=90)
				{
					add(voc[search[0]-65],search);
				}
				else if(search[0]>=97&&search[0]<=122)
				{	
					add(voc[search[0]-97],search);
				}
				else NULL;
			}
		}

		if(control==3)
		{
			printf("삭제할 단어를 입력하세요:\n");
			scanf("%s",search);
			if(rem(voc,search)<26)
			{
	 			printf("->삭제되었습니다.\n");
			}
			else printf("->단어장에 없습니다.\n");
		}

		if(control==4)
		{
			printf("단어장에서 찾을 단어를 입력하세요:\n");
			scanf("%s",search);
			if(find(voc,search)==0)
 			{
 				printf("단어장에 없습니다.\n");
			}
			if(find(voc,search)==1)
			{
				printf("단어장에 있습니다.\n");
			}
		}

		if(control==5)
		{
			f=fopen("voc.txt","wt");
			i=0;
			for(i=0;i<26;i++)
			{
				t=voc[i]->header;
				while(t!=NULL)
				{
					fputs(t->data,f);
					fputs("\n",f);
					t=t->link;
				}
			}
			fclose(f);
		}
		if(control==6)
		{
			f=fopen("voc.txt","wb");
			i=0;
			for(i=0;i<26;i++)
			{
				t=voc[i]->header;
				while(t!=NULL)
				{
					fputs(t->data,f);
					fputs("\n\r",f);
					t=t->link;
				}
			}
			fclose(f);
		}
		if(control==7)
			break;
	}
	i=0;
	for(i=0;i<26;i++)
	{
		del=array[i]->header;
		
		while(del!=NULL)
		{  
			tem=del->link;
			free(del);
			del=tem;
		}
		free(tem);
	}
	i=0;
	for(i=0;i<26;i++)
	{
		free(array[i]);
	}
	i=0;
	for(i=0;i<26;i++)
	{
		del=voc[i]->header;
		
		while(del!=NULL)
		{  
			tem=del->link;
			free(del);
			del=tem;
		}
		free(tem);
	}
	i=0;
	for(i=0;i<26;i++)
	{
		free(voc[i]);
	}
	return 0;
}			