#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define BUFSIZE 1000
#define TRUE  1
#define FALSE 0
#define WORD_LENGTH 5
#define ALPHABET_NUM 26
#define DICT_FLAG 0
#define VOCA_FLAG 1

typedef struct ListNodeType
{
	char *data;
	struct ListNodeType* pLink;
}ListNode;

typedef struct LinkedListType
{
	int currentElementCount;
	ListNode headerNode;
}LinkedList;

LinkedList* createLinkedList();
int addLLElement(LinkedList* pList, int position, ListNode element);
int removeLLElement(LinkedList* pList, int position);
int getElementPosition(LinkedList* pList, ListNode element);
ListNode* getLLElement(LinkedList* pList, int position);
void displayLinkedList(LinkedList* pList);

int menu_selector();
void file_insert_ascii_word(LinkedList* pList[], FILE* f);
void file_insert_binary_word(LinkedList* pList[], FILE* f);
int insert_word(LinkedList* pList[], char* word, int flag);
int delete_word(LinkedList* pList[]);
void print_vocabulary(LinkedList* pList[]);
void search_word(LinkedList* pList[], LinkedList* v_pList[], int flag);
void onlyalpha(char* word, char* dest);

void store_in_ascii(LinkedList* pList[]);
void store_in_binary(LinkedList* pList[]);


int main(int argc, char* argv[])
{
	int i = 0;

	LinkedList* pList[ALPHABET_NUM];
	LinkedList* v_pList[ALPHABET_NUM];
	
	int select_menu;
	char* word;
	for(i = 0; i < ALPHABET_NUM; i++)
	{
		pList[i] = createLinkedList();
		v_pList[i] = createLinkedList();
	}

	FILE *f;
	f = fopen("voc_sample.txt", "r");
	
	FILE* fb;
	fb = fopen("voc.txt", "rb");
	//file_insert_word(pList, f);
	if(pList != NULL)
	{	
		file_insert_ascii_word(pList, f);
		file_insert_binary_word(pList, fb);
		while(1)
		{
			word = (char *)malloc(sizeof(char) * WORD_LENGTH);
			select_menu = menu_selector();
			switch(select_menu)
			{	
				// [0] Search from Dictionary
				case 0:
					search_word(pList, v_pList, DICT_FLAG);
					break;	
				// [1] Search from Vocabulary
				case 1:
					search_word(pList, v_pList, VOCA_FLAG);
					break;

				// [2] Insert
				case 2:
					system("clear");
					printf("┌─────────────────────────────────────┐\n");
					printf("│[2] Insert                           │\n");
					printf("│Write a word to replace in vocabulary│\n");
					printf("└─────────────────────────────────────┘\n");
					printf("=> ");
					scanf("%s", word);
					insert_word(v_pList, word, VOCA_FLAG);
					break;

				// [3] Delete
				case 3:
					delete_word(v_pList);
					break;
				
				// [4] Print List
				case 4:
					print_vocabulary(v_pList);
					break;

				// [5] Store in ASCii
				case 5:
					store_in_ascii(v_pList);
					break;

				// [6] Store in Binary
				case 6:
					store_in_binary(v_pList);
					break;
				// [-1] Quit
				case -1:
					system("clear");
					printf("Program is terminated\n");
					return 0;

				default:
					system("clear");
					printf("Input a right option value!\n");
					break;
			}
		}
	}

	fclose(fb);
	fclose(f);
	return 0;
}

int menu_selector()
{
	printf("\n");
	printf("┌─────────────────────┐\n");
	printf("│  Select a Function  │\n");
	printf("│  [0] Search in Dict │\n");
	printf("│  [1] Search in Voca │\n");
	printf("│  [2] Insert         │\n");
	printf("│  [3] Delete         │\n");
	printf("│  [4] Print List     │\n");
	printf("│  [5] Store in ASCii │\n");
	printf("│  [6] Store in Binary│\n");
	printf("│  [-1] Quit          │\n");
	printf("└─────────────────────┘\n");
	printf("=> ");

	int select_num;
	scanf("%d", &select_num);
	
	return select_num;
}

void file_insert_ascii_word(LinkedList* pList[], FILE* f)
{
	int char_pos, i = 0, count;

	char_pos = 'A';
	char* buf = (char *)malloc(sizeof(char) * BUFSIZE);

	while(EOF != fscanf(f, "%s", buf))
		insert_word(pList, buf, DICT_FLAG);

}


void file_insert_binary_word(LinkedList* pList[], FILE* f)
{
	int char_pos, i = 0, count = 1;
	char_pos = 'A';
	char* word = (char *)malloc(sizeof(char) * count);
	char buf;

	while('\0' != fread(&buf, sizeof(char), 1, f))
	{
		if(buf != '\x0a')
		{
			word[count - 1] = buf;
			count++;
			word = (char *)realloc(word, sizeof(char) * count);
		}
		
		else
		{
			insert_word(pList, word, DICT_FLAG);
			memset(word, 0, sizeof(char) * count);
			count = 1;
			word = (char *)realloc(word, sizeof(char) * count);
		}
	}
	free(word);
		
}



void search_word(LinkedList* pList[], LinkedList* v_pList[], int flag)
{
	int char_pos, judge;
	ListNode node;
	memset(&node, 0, sizeof(ListNode));
	node.data = (char *)malloc(sizeof(char) * WORD_LENGTH);

	system("clear");
	printf("┌─────────────────────────────────────┐\n");
	printf("│[%d] Search                           │\n", flag);

	if(flag == DICT_FLAG)
		printf("│Write a word to find in Dictionary   │\n");
	else
		printf("│Write a wrod to find in Vocabulary   │\n");

	printf("└─────────────────────────────────────┘\n");
	printf("=> ");
	scanf("%s", node.data);
	
	if(!isalpha(node.data[0]))
	{
		printf("This is not a word!\n");
		return;		
	}

	else
	{
		if('a' <= node.data[0] && node.data[0] <= 'z')
			node.data[0] = toupper(node.data[0]);
		char_pos = node.data[0] - 'A';
		if(flag == DICT_FLAG)
			judge = getElementPosition(pList[char_pos], node);
		else if(flag == VOCA_FLAG)
			judge = getElementPosition(v_pList[char_pos], node);
		else
			printf("[Error]\n");

		if(judge > -1 && flag == DICT_FLAG)
		{
			printf("[DICT] Find \"%s\" at \'%c\' [%d]\n", node.data, char_pos + 'A', judge);
			insert_word(v_pList, node.data, flag);
			return ;
		}

		else if(judge > -1 && flag == VOCA_FLAG)
		{
			printf("[VOCA] Find \"%s\" at \'%c\' [%d]\n", node.data, char_pos + 'A', judge);
			return ;
		}

		
		else
		{
			printf("Cannot find at \'%c\' vocabulary\n", char_pos + 'A');
			return ;
		}
	}
}


int insert_word(LinkedList* pList[], char* word, int flag)
{
	int char_pos, i;
	char_pos = 'A';
	ListNode node;
	memset(&node, 0, sizeof(ListNode));
	node.data = (char *)malloc(sizeof(char));
	strcpy(node.data, word);
	
	if(!isalpha(node.data[0]))
	{
		if(flag == VOCA_FLAG)
			printf("This is not a word!\n");
		return -1;		
	}

	if('a'<=node.data[0] && node.data[0] <= 'z')
		node.data[0] = toupper(node.data[0]);
	char_pos = node.data[0] - 'A';

	if(getElementPosition(pList[char_pos], node) > -1 && flag == VOCA_FLAG)
		printf("Duplicated word at \'[%c]\' [%d]\n",char_pos + 'A', getElementPosition(pList[char_pos], node));

	else if(getElementPosition(pList[char_pos], node) <= -1)
		addLLElement(pList[char_pos], pList[char_pos]->currentElementCount, node);
	return 0;
}

int delete_word(LinkedList* pList[])
{
	int i, char_pos, pos = 0;
	ListNode node;
	memset(&node, 0, sizeof(ListNode));
	node.data = (char *)malloc(sizeof(char) * WORD_LENGTH);

	system("clear");
	printf("┌──────────────────────────────────────┐\n");
	printf("│[3] Delete                            │\n");
	printf("│Write a word to delete from vocabulary│\n");
	printf("└──────────────────────────────────────┘\n");
	printf("=> ");
	scanf("%s", node.data);
	
	char_pos = 'A';

	if('a' <= node.data[0] && node.data[0] <= 'z')
		node.data[0] = toupper(node.data[0]);
	char_pos = toupper(node.data[0]) - 'A';
	pos = getElementPosition(pList[char_pos], node);
	
	if(pos > -1)
		removeLLElement(pList[char_pos], pos);		

	else
		printf("Cannot delete \"%s\" at vocabulary\n", node.data);
	
	return 0;
}

void print_vocabulary(LinkedList* pList[])
{
	int i, voc_count = 0;
	for(i = 0; i < ALPHABET_NUM; i++)
	{
		if(pList[i]->currentElementCount > 0)
		{
			printf("[*][%c] Vocabulary \n", 'A' + i);
			displayLinkedList(pList[i]);
		}
	}
}

void onlyalpha(char* word, char* dest)
{
	int i, j = 0;
	for(i = 0; i <= strlen(word); i++)
	{
		if(('a' <= word[i] && word[i] <= 'z') || ('A' <= word[i] && word[i] <= 'Z'))
			dest[j++] = word[i];
			
	}
}

void store_in_ascii(LinkedList* pList[])
{
	FILE* f;
	f = fopen("ASCii_voc.txt", "w");
	int i, j, f_size;
	for(i = 0; i < ALPHABET_NUM; i++)
	{
		if(pList[i]->currentElementCount > 0)
		{
			fprintf(f, "[*][%c] Vocabulary \r\n", 'A' + i);
			for(j = 0; j < pList[i]->currentElementCount; j++)
				fprintf(f, "[-][%d]: %s\r\n", j, getLLElement(pList[i], j)->data);
			fprintf(f, "\r\n");
		}
	}

	fseek(f, 0, SEEK_END);
	f_size = ftell(f);

	if(f_size != 0)
	{
		printf("\n아스키파일 단어장이 저장되었습니다.\n");
		printf("파일 이름: ASCii_voc.txt\n");
	}

	else
		printf("아스키파일 단어장 저장에 실패하였습니다.\n");
	fclose(f);
}

void store_in_binary(LinkedList* pList[])
{
	FILE* f;
	f = fopen("Binary_voc.bin", "wb");
	int i, j, f_size;
	
	char* word_end = "\r\n";
	char* title = (char *)malloc(sizeof(char));
	char* word = (char *)malloc(sizeof(char));

	for(i = 0; i < ALPHABET_NUM; i++)
	{
		if(pList[i]->currentElementCount > 0)
		{
			sprintf(title, "[*][%c] Vocabulary \r\n", 'A' + i);
			fwrite(title, strlen(title), 1, f);
			for(j = 0; j < pList[i]->currentElementCount; j++)
			{
				sprintf(word, "[-][%d]: %s\r\n", j, getLLElement(pList[i], j)->data);
				fwrite(word, strlen(word), 1, f);
			}
			fwrite(word_end, strlen(word_end), 1, f);

		}
	}

	fseek(f, 0, SEEK_END);
	f_size = ftell(f);

	if(f != 0)
	{
		printf("\n이진파일 단어장이 저장되었습니다.\n");
		printf("파일 이름: Binary_voc.bin\n");
	}
	else
		printf("\n이진파일 단어장 저장에 실패하였습니다.\n");
	
	fclose(f);
}
LinkedList* createLinkedList()
{
	LinkedList* pReturn = NULL;
	int i = 0;

	pReturn = (LinkedList *)malloc(sizeof(LinkedList));
	if(pReturn != NULL)
		memset(pReturn, 0, sizeof(LinkedList));
	else
	{
		printf("[0] MEMORY ALLOCATE ERROR\n");
		return NULL;
	}

	return pReturn;
}

int addLLElement(LinkedList* pList, int position, ListNode element)
{
	int ret = FALSE;
	int i = 0;
	ListNode* pPreNode = NULL;
	ListNode* pNewNode = NULL;
	
	if(pList != NULL)
	{
		if(0 <= position && position <= pList->currentElementCount)
		{
			pNewNode = (ListNode *)malloc(sizeof(ListNode));
			if(pNewNode != NULL)
			{
				*pNewNode = element;
				pNewNode->pLink = NULL;

				pPreNode = &(pList->headerNode);
				for(i = 0; i < position; i++)
					pPreNode = pPreNode->pLink;

				pNewNode->pLink = pPreNode->pLink;
				pPreNode->pLink = pNewNode;

				pList->currentElementCount++;

				ret = TRUE;
			}

			else
			{
				printf("오류, 메모리 할당 addLLElement()\n");
				return ret;
			}
		}

		else
			printf("오류, 위치 인덱스 - [%d], addLLElement()\n", position);
	}
	return ret;
}

int removeLLElement(LinkedList* pList, int position)
{
	int ret = FALSE;
	int i = 0;
	int arrayCount = 0;

	ListNode* pNode = NULL;
	ListNode* pDelNode = NULL;

	if(pList != NULL)
	{
		arrayCount = pList->currentElementCount;	
		if(0 <= position && position < arrayCount)
		{
			pNode = &(pList->headerNode);
			for(i = 0; i < position; i++)
				pNode = pNode->pLink;

			pDelNode = pNode->pLink;
			pNode->pLink = pDelNode->pLink;

			free(pDelNode);

			pList->currentElementCount--;
			
			ret = TRUE;
		}

		else
			printf("오류, 위치 인덱스 - [%d] removeLLElement()\n", position);
	}
	return ret;
}

ListNode* getLLElement(LinkedList* pList, int position)
{
	int i = 0;

	ListNode* pReturn = NULL;
	ListNode* pNode = NULL;

	if(pList != NULL)
	{
		if(0 <= position && position < pList->currentElementCount)
		{
			pNode = &(pList->headerNode);
			for(i = 0; i <= position; i++)
				pNode = pNode->pLink;

			pReturn = pNode;
		}
	}
	return pReturn;
}

void displayLinkedList(LinkedList* pList)
{
	int i = 0;
	if(pList != NULL)
	{	
		for(i = 0; i < pList->currentElementCount; i++)
			printf("[-][%d]: %s\n", i, getLLElement(pList, i)->data);	
		printf("\n");
	}
}

int getElementPosition(LinkedList* pList, ListNode element)
{
	int i;
	if(pList->currentElementCount == 0)
		return -2;
	
	for(i = 0; i < pList->currentElementCount; i++)
	{
		if(strcmp(getLLElement(pList, i)->data, element.data) == 0)
			return i;
	}	
	return -1;
}

