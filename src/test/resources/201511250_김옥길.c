#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef struct NODE
{
	char alpha;
	char word[];
	struct NODE *link;
}NODE;

void Dictionary Search(NODE *sample_list, FILE *dic);
void search(NODE *voc_list, FILE *dic):
void store_in_ASCII(NODE *voc_list);
void store_in_binary(NODE *voc_list);
void delete(NODE *voc_list);
void Quit();

int main(void)
{
	FILE *fp;
	char buffer[];
	char word_list[][];
	int x=0, y=0;

	int number;  // 기능 선택

	int i, j;


	// 사전 (일반)파일열기
	fp = fopen("sample.txt", "r");

	// 사전 단어분리
	while(fgets(buffer, sizeof(buffer), fp))
	{
		for(i=0; i<sizeof(buffer); i++)
		{
			if(isalpha(buffer[i]))
			{
				if(isupper(buffer[i]))
					tolower(buffer[i]);
				strcpy(word_list[x][y], buffer[i]);
				y++;
			}
			else if(isspace(buffer[i]))
			{
				x++;
				y=0;
			}
		}
	}

	// 사전 단어정렬
	NODE *sample_list;
	sample_list = (NODE *)calloc(x*sizeof(NODE));

	sample_list->link = NULL;

	for(i=0; i<x; i++)
	{
		int alp_num = 0;
		int alp_last = 0;

		// 알파벳 당 노드 개수 구하기
		for(j=0; j<x; j++)
		{
			if(strcmp((voc_list+j)->alpha, word_list[i][0])==0)
			{
				alp_num++;
				alp_last = j;
			}
		}

		// 단어 삽입
		(sample_list+i)->alpha = word_list[i][0];
		(sample_list+i)->word = word_list[i][];

		(sample_list+i)->link = (voc_list+alp_last)->link;
		(sample_list+alp_last)->link = (sample_list+i);
	}
	fclose(fp);

	// 단어장 (일반)파일열기
	fp = fopen("voc.txt", "r");

	// 단어장 단어분리
	while(fgets(buffer, sizeof(buffer), fp))
	{
		for(i=0; i<sizeof(buffer); i++)
		{
			if(isalpha(buffer[i]))
			{
				if(isupper(buffer[i]))
					tolower(buffer[i]);
				strcpy(word_list[x][y], buffer[i]);
				y++;
			}
			else if(isspace(buffer[i]))
			{
				x++;
				y=0;
			}
		}
	}

	// 단어장 단어정렬
	NODE *voc_list;
	sample_list = (NODE *)calloc(x*sizeof(NODE));

	sample_list->link = NULL;

	for(i=0; i<x; i++)
	{
		int alp_num = 0;
		int alp_last = 0;

		// 알파벳 당 노드 개수 구하기
		for(j=0; j<x; j++)
		{
			if(strcmp((voc_list+j)->alpha, word_list[i][0])==0)
			{
				alp_num++;
				alp_last = j;
			}
		}

		// 단어 삽입
		(sample_list+i)->alpha = word_list[i][0];
		(sample_list+i)->word = word_list[i][];

		(sample_list+i)->link = (voc_list+alp_last)->link;
		(sample_list+alp_last)->link = (sample_list+i);
	}
	fclose(fp);

	FILE *dic = fopen("dic", "w");
	fwrite(voc_list, sizeof(NODE), sizeof(voc_list), dic);


	// 메뉴
	printf("기능을 선택하세요\n");
	printf("[1] Dictionary Search  [2] search\n");
	printf("[3] store in ASCII  [4] store in binary\n");
	printf("[5] delte  [6] Quit\n");

	do
	{
		number = 0;

		printf(": ");
		scanf("%d", &number);

		switch(number)
		{
			case 1:
			 Dictionary Search(sample_list, dic);
			 break;
			case 2:
			 search(voc_list, dic);
			 break;
			case 3:
			 store_in_ASCII(voc_list);
			 break;
 			case 4:
			 store_in_binary(voc_list);
			 break;
			case 5:
			 delete(voc_list);
			 break;
			case 6:
			 Quit();
			 break;
			default:
			 printf("잘못된 입력입니다.\n");
			 break;
		}
	}while(number!=6);

	free(sample_list);	
	free(voc_list);
	fclose(fp);

	return 0;
}

void Dictionary Search(NODE *sample_list, FILE *dic)
{
	NODE *p;
	char searchWord[];
	int bin = 0;

	p = (sample_list+i);

	printf("찾을 단어를 입력하세요: ");
	scanf("%s", searchWord);

	while(p)
	{
		if(strstr(p->word, searchWord))
		{
			printf("-> 단어장에 있습니다.\n");
			fwrite(searchWord, size(NODE), sizeof(searchWord), dic);
			bin = 1;
			break;
		}

		p = p->link;
	}
	if(bin!=1)
		printf("-> 사전에 없습니다.\n");

	free(p);
}

void search(NODE *voc_list, FILE *dic)
{
	NODE *p;
	char searchWord[];

	printf("찾을 단어를 입력하세요: ");
	scanf("%s", searchWord);

	while(fread(p, sizeof(NODE), sizeof(voc_list), dic))
	{
		if(strstr(p->word, searchWord))
		{
			printf("-> 단어장에 있습니다.\n");
			bin = 1;
			break;
		}

		p = p->link;
	}
	if(bin!=1)
		printf("-> 단어장에 없습니다.\n");

	free(p);
}

void store_in_ASCII(NODE *voc_list)
{
	char insertWord[];
	int i, j;

	printf("삽입할 단어를 입력하세요: ");
	scanf("%s", insertWord);

	for(i=0; i<x; i++)
	{
		if(strcmp((voc_list+i)->word, insertWord)!=0)
		{
			int alp_num = 0;
			int alp_lsat = 0;
			int null_be = -1;
			int bin = 0;

			// 알파벳 당 노드 개수 구하기
			for(j=0; j<x; j++)
			{
				if(strcmp((voc_list+j)->alpha, insertWord[0])==0)
				{
					alp_num++;
					alp_last = j;
				}
				if((voc_list+j)==NULL)
					null_be = j;
			}

			if(null_be != -1)
			{
				if(alp_num<NODE_NUM)
					(voc_list+null_be)->alpha = insertWord[0];
				else
					(voc_list+null_be)->alpha = '~'
				(voc_list+null_be)->word = insertWord;
				(voc_list+null_be)->link = (voc_list+j)->link;

				(voc_list+j)->link = (voc_list+null_be);
			}
			else
			{
				if(alp_num<NODE_NUM)
					(voc_list+x+1)->alpha = insertWord[0];
				else
					(voc_list+null_be)->alpha = '~'
				(voc_list+x+1)->word = insertWord;
				(voc_list+x+1)->link = (voc_list+j)->link;

				(voc_list+j)->link = (voc_list+x+1);
			}

			printf("-> 삽입했습니다.\n");
			bin = 1;
			break;
		}
	}
	if(bin!=1)
		printf("-> 삽입할 단어가 이미 있습니다.\n");

	fwrite(searchWord, sizeof(NODE), sizeof(searchWord), dic);	
}

void delete(NODE *voc_list)
{
	NODE *p;
	char deleteWord[];
	int i;

	p = (NODE *)calloc(sizeof(NODE));

	printf("삭제할 단어를 입력하세요: ");
	scanf("%s", deleteWord);

	for(i=0; i<x; i++)
	{
		int bin = 0;

		if(strcmp((voc_list+i)->word, deleteWord)==0)
		{
			p = (voc_list+i)->link;

			((voc_list+i)->link)->alpha = NULL;
			((voc_list+i)->link)->word = NULL;
			((voc_list+i)->link)->link = NULL;

			(voc_list+i)->alpha = p->alpha;
			(voc_list+i)->word = p->word;
			(voc_list+i)->link = p->link;

			printf("-> 삭제되었습니다.\n");
			bin = 1;
			break;
		}
	}
	if(bin!=1)
		printf("-> 삭제할 단어가 없습니다.\n");

	free(p);
}

void Quit()
{
	printf("종료되었습니다.\n");
}