#include "list.c"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define bool int
#define true 1
#define false 0

int menuVoc();
int menu();
void getal();
void insert();
void delete();
bool search();


typedef struct listNode{
	char element[256];
	struct listNode* next;
}node;

typedef struct linkedList{
	int count;
	struct listNode* head;
}list;

void init(list* lp){
	lp->count = 0;
	lp->head = (node*)malloc(sizeof(node));
	lp->head->next=NULL;
}

void addLastNode(list* lp, char* value){
	int i;
	node *new_np = NULL;
	node *tmp = lp->head;
	new_np = (node*)malloc(sizeof(node));
	new_np->next = NULL;
	
	for (i = 0; i < lp->count; i++) {
		tmp = tmp->next;
	}
	tmp->next = new_np;
	strcpy(new_np->element,value);
	lp->count++;
}

void del(list* lp, char* value) {
	node *tmp = lp->head;
        int i;
        for(i = 0; i < lp->count; i++) {
                if(!strcmp(tmp->next->element, value)) {
                        if(i==lp->count-1) {
				tmp->next = NULL;
				lp->count--;
				//printf("지움 확인");
				return;
			}
			else {
				tmp->next = tmp->next->next;
				lp->count--;
				return;
				//printf("지움 확인");
			}
                }
                tmp = tmp->next;
        }
        printf("단어장에 없습니다.\n");
}

bool sea(list *lp, char* value) {
	node *tmp = lp->head->next;
	int i;
	for(i = 0; i < lp->count; i++) {
		if(!strcmp(tmp->element, value)) {
			//printf("단어장에 있습니다.\n");
			return true;
		}
		tmp = tmp->next;
	}
	//printf("단어장에 없습니다.\n");
	return false;
}
/*
void main() {
	list* a = (list*)malloc(sizeof(list));
	init(a);
	sea(a, "apple");	
	addLastNode(a,"apple");
	printf("%s\n", a->head->next->element);
	sea(a, "apple");
	del(a, "apple");
	del(a, "apple");
	//printf("%s\n", a->head->next->element);
	sea(a, "apple");
}*/




void main(void) {
	list *dic = (list*)malloc(sizeof(list));
	list *voc = (list*)malloc(sizeof(list));
	char word[256];
	int option;
	int i;
	FILE *fdp = NULL;
	FILE *fvp = NULL;
	//fvp 2개?
	
	init(dic);
	init(voc);
	
	fdp = fopen("sample.txt","rb");
	while(fscanf(fdp, "%s", word)>0) {
		insert(dic, word);
	}
	fclose(fdp);

	while(true) {
		option = menu();

		if(option == 1) {
			printf("사전에서 찾을 단어를 입력하세요 : \n");
			scanf("%s", word);
			//search() 에서 세그멘테이션

			if(search(dic, word)) {
				printf("사전에 단어가 있습니다.\n");
				printf("단어장에 단어를 넣습니다.\n");

				insert(voc, word);
			}
			else {
				printf("사전에 단어가 없습니다.\n");
			}
		}
		else if(option == 2) {
			while(true) {
				option = menuVoc();
				if(option == 1) {
					printf("단어장에서 찾을 단어를 입력하세요.\n");
					scanf("%s", word);
					search(voc, word);
				}
				else if(option == 2) {
					printf("단어장에 넣을 단어를 입력하세요.\n");
					scanf("%s",word);
					insert(voc, word);
				}
				else if(option == 3) {
					printf("단어장에서 지울 단어를 입력하세요.\n");
					scanf("%s", word);
					delete(voc, word);
				}
				else if(option == 4) {
					i = 0;
					node *tmp = voc->head->next;
					fvp = fopen("voca.txt", "w");
					while(i != voc->count) {
						fprintf(fvp, "%s",tmp->element);
						i++;
						tmp = tmp->next;
					}
					fclose(fvp);
				}
				else if(option == 5) {
					i = 0;
					node *tmp = voc->head->next;
					fvp = fopen("voca.txt", "wb");
					while(i != voc->count) {
						fprintf(fvp, "%s", tmp->element);
						i++;
						tmp = tmp->next;
					}
					fclose(fvp);
				}
				else {//option == 6
					printf("사전 메뉴로 나갑니다.\n");
					break;
				}
			}
		}
		else //option == 3
			break;
	}
	
}

int menuVoc() {
	int option;
	printf("단어장의 기능을 선택하세요 : \n");
	printf("[1] Search 		[2] Insert\n");
	printf("[3] Delete 		[4] Store int ASCII\n");
	printf("[5] Store in binary 	[6] Quit\n");

	scanf("%d", &option);

	while(option > 6 && option < 1) {
		printf("잘못 입력하셨습니다. 다시 입력해주세요.\n");
		return menuVoc();
	}
	return option;
}

int menu() {
	int option;
	printf("기능을 선택하세요 : \n");
	printf("[1] 사전 단어 검색\n");
	printf("[2] 단어장 기능\n");
	printf("[3] Quit");

	scanf("%d", &option);
	while(option > 3 && option < 1) {
		printf("잘못 입력하셧습니다. 다시 입력하세요.\n");
		return menu();
	}
	return option;
}


void getal(char *pDest, const char *pSrc) {
        char *pTemp = pDest;

        if(pDest==NULL || pSrc == NULL) return;

        while(*pSrc != '\0') {
                if(!isalpha(*pSrc))
                        pSrc++;
                else
                        *pTemp++ = *pSrc++;
        }
        *pTemp = '\0';

}

void insert(list *lp, char *word) {
	getal(word,word);
	int i = 0;
	while(word[i] != '\0') {
		if(isupper(word[i]))
			word[i] = tolower(word[i]);
		i++;
	}
	if(strlen(word) == 0) 
		return;
	if(!sea(lp, word)) {
		addLastNode(lp,word);
	}
}

void delete(list *lp, char *word) {
	getal(word,word);
	int i = 0;
	while(word[i] != '\0') {
		if(isupper(word[i]))
			word[i] = tolower(word[i]);
		i++;
	}

	if(sea(lp,word))
		del(lp,word);
}

bool search(list *lp, char *s_word) {
	getal(s_word, s_word);
	int i = 0;
	while(s_word[i] != '\0') {
		if(isupper(s_word[i]))
			s_word[i] = tolower(s_word[i]);
		i++;
	}

	if(sea(lp, s_word)) 
		return true;
	
	return false;
}
