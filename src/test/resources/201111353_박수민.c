#include <stdio.h>
#include <memory.h>
#include <string.h>
#include <malloc.h>
#include <ctype.h>
#include <stdlib.h>
#define TRUE	1
#define FALSE	0
#define SIZE 100
#pragma warning (disable: 4996)
typedef struct tagWordListNode
{
	char*			word;
	struct tagWordListNode*	prev;
	struct tagWordListNode*	next;
} WordListNode;

typedef struct tagWordList
{
	WordListNode	head;
	int				count;
} WordList;

typedef struct tagDictionary
{
	WordList	wordLists[27];
} Dictionary;

typedef struct vocabulary {
	char word[SIZE];
}vocabulary;

int WLInit(WordList* list);
int WLIsExist(WordList* list, char* word);
int WLAddWord(WordList* list, char* word);
int WLRemoveWord(WordList* list, char* word);
int WLPrintWord(WordList* list, int num);

int DicInit(Dictionary* dic);
int DicIsExist(Dictionary* dic, char* word);
int DicAddWord(Dictionary* dic, char* word);
int DicRemoveWord(Dictionary* dic, char* word);
int DicPrintWord(Dictionary* dic);

int Word(char* word, char* regex);
char* GetWord(char* _line);
void RemoveLineEndCharacter(char* line);

void StoreInBinary(Dictionary* voca, FILE* fp1);
void SerachDicStoreVoca(Dictionary *dic, Dictionary *voca, FILE* fp, FILE* fp1, char* word);	

int main()
{
	char* word;
	char* real_word = NULL;
	char search_word[30];
	char search_store_word[30];
	char insert_word[30];
	char delete_word[30];
	FILE *fp;
	FILE *fp1;
	char line[400];
	int i = 0;
	int len;
	int select;
	int result = FALSE;
	int pos;
	Dictionary dic;
	Dictionary voca;	
	DicInit(&dic);
	DicInit(&voca);	
	vocabulary data;
	fp = fopen("sample.txt", "r");
	fseek(fp, 0, SEEK_SET);
	while (fgets(line, 400, fp) != NULL)
	{
		RemoveLineEndCharacter(line);		// \n, \r 제거를 위해서... 제거 안하면 결과가 조금 이상해짐
		word = GetWord(line);
		while (word != 0)                  // 한 줄에서 단어가 없을 때까지...
		{
			len = strlen(word);
			if (isalnum(word[len - 1]))			//if (isalpha(word[len - 1]) || isdigit(word[len - 1]))
			{
				if (isalnum(word[0]))
				{
					DicAddWord(&dic, word);
				}
				else
				{
					pos = 0;
					while (1)
					{
						pos = pos + 1;
						if (isalnum(word[pos]))
							break;
					}
					real_word = (char *)malloc(len - pos);
					for (i = 0; i < len - pos; i++)
					{
						real_word[i] = word[pos + i];
					}
					real_word[len - pos] = '\0';
					DicAddWord(&dic, real_word);
				}

			}
			else
			{
				while (1)
				{
					len = len - 1;
					if (isalnum(word[len - 1]))			//if ((isalpha(word[len - 1])) || isdigit(word[len - 1]))
						break;
				}
				if (isalnum(word[0]))
				{
					real_word = (char *)malloc(len);
					strncpy(real_word, word, len);
					real_word[len] = '\0';
					DicAddWord(&dic, real_word);
				}
				else
				{
					pos = 0;
					while (1)
					{
						pos = pos + 1;
						if (isalnum(word[pos]))
							break;
					}
					real_word = (char *)malloc(len - pos);
					for (i = 0; i < len - pos; i++)
					{
						real_word[i] = word[pos + i];
					}
					real_word[len - pos] = '\0';
					DicAddWord(&dic, real_word);
				}

			}
			word = GetWord(NULL);
		}
	}

	fp1 = fopen("voc.txt", "r+");
	fseek(fp1, 0, SEEK_SET);
	fflush(stdin);
	while (!feof(fp1)) {
		fread(&data, sizeof(data), 1, fp1);
		DicAddWord(&voca, data.word);
	}
	fclose(fp1);
	printf("---------------------------------------------------------\n");
	printf("| 1~3번 기능은 단어장의 리스트 기능입니다.              |\n");
	printf("| 4번 기능을 통해 저장할 수 있습니다.                   |\n");
	printf("| insert, delete후 저장을 안하면 단어장에 존재X         |\n");
	printf("| 5번 -> 사전에 단어가 존재할 경우 단어장에 바로 저장   |\n");
	printf("---------------------------------------------------------\n");

	while (1)
	{
		
		fseek(fp, 0, SEEK_SET);
		fseek(fp1, 0, SEEK_SET);
		

		printf("===============Dictionary & Vocabulary===============\n");
		printf("| [1] Search (단어장 리스트에서 검색)               |\n");
		printf("| [2] Insert (단어장 리스트에서 삽입)               |\n");
		printf("| [3] Delete (단어장 리스트에서 삭제)               |\n");
		printf("| [4] Store in binary(Vocabulary) (단어장에 저장)   |\n");
		printf("| [5] Search in Dictionary and store at Vocabulary  |\n");
		printf("| [6] Print Dictionary List (사전 리스트 보기)      |\n");
		printf("| [7] Print Vocabulary List (단어장 리스트 보기)    |\n");
		printf("| [8] Quit (종료)                                   |\n");
		printf("=====================================================\n");
		printf("기능을 선택하세요 : [ ]\b\b");
		
		scanf("%d", &select);
		fflush(stdin);
		
		
		switch (select)
		{
	
		case 1:
			printf("찾을 단어를 입력하세요(소문자로만 검색가능!) : ");
			scanf("%s", search_word);
			result = DicIsExist(&voca, search_word);
			if (result == TRUE)
				printf("-> 단어장에 있습니다.\n");
			else
				printf("-> 단어장에 없습니다.\n");
			result = FALSE;
			fflush(stdin);
			break;

		case 2:
			printf("추가할 단어를 입력하시오 : ");
			scanf("%s", insert_word);
			result = DicAddWord(&voca, insert_word);
			if (result == TRUE)
				printf("-> 단어가 추가되었습니다\n");
			else
				printf("-> 이미 존재하는 단어입니다\n");
			result = FALSE;
			fflush(stdin);
			break;

		case 3:
			printf("삭제할 단어를 입력하시오 : ");
			scanf("%s", delete_word);
			result = DicRemoveWord(&voca, delete_word);
			if (result == TRUE)
				printf("-> 삭제되었습니다\n");
			else
				printf("->존재하지 않는 단어입니다\n");
			result = FALSE;
			fflush(stdin);
			break;

		case 4:
			fp1 = fopen("voc.txt", "w+");		// 파일이 존재하면 새롭게 생성! 현재 list에 있는 단어들을 전부 다 write하기 위해서!
			printf("현재 Voca list에 있는 모든 단어를 저장합니다!\n");
			StoreInBinary(&voca, fp1);
			fclose(fp1);
			break;

		case 5 :
			fp1 = fopen("voc.txt", "r+");		
			printf("사전에서 찾을 단어를 입력하세요 : ");
			scanf("%s", search_store_word);
			SerachDicStoreVoca(&dic, &voca, fp, fp1, search_store_word);
			fseek(fp1, 0, SEEK_SET);
			fflush(stdin);
			while (!feof(fp1)) {
				fread(&data, sizeof(data), 1, fp1);
				DicAddWord(&voca, data.word);
			}
			fclose(fp1);
			break;

		case 6 :
			printf("[6] Print Dictionary List\n");
			DicPrintWord(&dic);
			break;

		case 7 :
			printf("[7] Print Vocabulary List\n");
			DicPrintWord(&voca);
			break;
		case 8:
			exit(1);
			break;

		default:
			printf("잘못 입력하셨습니다!\n");
			break;
		}
		result = FALSE;
		
	}
	fclose(fp);
	//fclose(fp1);
	return 0;
}
void StoreInBinary(Dictionary* voca, FILE* fp1)
{
	int i;
	int count = -1;
	WordList* temp;
	WordListNode* head = NULL;
	WordListNode* current = NULL;
	vocabulary data;

	for (i = 0; i < 26; i++)
	{
		temp = &voca->wordLists[i];
		head = &temp->head;
		current = head->next;

		while (current != head)
		{
				strcpy(data.word, current->word);
				fseek(fp1, 0, SEEK_END);
				fwrite(&data, sizeof(data), 1, fp1);
				current = current->next;
			}
		}

}
void SerachDicStoreVoca(Dictionary *dic, Dictionary *voca, FILE* fp, FILE* fp1, char* word)
{
	int result1;
	vocabulary data;

	result1 = DicIsExist(dic, word);
	if (result1 == TRUE)
	{
		printf("-> 사전에 있습니다.\n");
		//fseek(fp1, 0, SEEK_SET);
		fflush(stdin);
		
		while (!feof(fp1)) {
			fread(&data, sizeof(data), 1, fp1);
			if (strcmp(data.word, word) == 0) {
				printf("단어장에 이미 존재하는 단어입니다\n");
				break;
			}
			if (feof(fp1))
			{
				printf("단어장에 일치하는 단어 없습니다!!\n");
				strcpy(data.word, word);
				fseek(fp1, 0, SEEK_END);
				fwrite(&data, sizeof(data), 1, fp1);
				printf("단어장에 단어 추가되었습니다.!\n");
				break;
			}
		}

	}
	else
		printf("-> 사전에 없습니다.\n");
	result1 = FALSE;
	fflush(stdin);

}
void RemoveLineEndCharacter(char* line)
{
	int i;
	int len = strlen(line);
	for (i = 0; i < len; ++i)
	{
		if (line[i] == '\r' || line[i] == '\n')
			line[i] = 0;
	}
}

char* GetWord(char* _line)
{
	static char* line = NULL;
	static int line_len = 0;
	static int prev_pos = 0;
	static int pos = 0;
	char* ret = 0;
	int ri;
	int i;

	if (_line != '\0')
	{
		line = _line;
		line_len = strlen(_line);
		pos = 0;
	}
	while (line[pos] == ' ')
	{
		pos++;
		if (line[pos] == '\0')
			return NULL;
	}
	prev_pos = pos;
	while (line[pos] != ' ')
	{
		pos++;
		if (line[pos] == '\0')
		{
			if (prev_pos == pos - 1) return NULL;
			else break;
		}
	}

	ret = (char*)malloc(sizeof(char) * (pos - prev_pos + 1));

	ri = 0;
	for (i = prev_pos; i < pos; ++i)
	{
		ret[ri++] = line[i];
	}
	ret[ri] = '\0';
	return ret;
}

int DicInit(Dictionary* dic)
{
	int i;
	for (i = 0; i < 27; ++i)
	{
		WLInit(&dic->wordLists[i]);
	}

	return TRUE;
}

int DicIsExist(Dictionary* dic, char* word)
{
	int i;
	for (i = 0; i < 27; ++i)
	{
		if (WLIsExist(&dic->wordLists[i], word))
			return TRUE;
	}
	return FALSE;
}
int DicAddWord(Dictionary* dic, char* word)
{
	WordList* current;
	int wanted_index = tolower(word[0]) - 'a';
	if (wanted_index > 25 || wanted_index < 0)
		wanted_index = 26;

	int alter_index = 26;
	int i;

	for (i = 0; word[i] != 0; ++i)
		word[i] = tolower(word[i]);

	current = &dic->wordLists[wanted_index];
	//if (WLIsExist(current, word) == FALSE && current->count >= 5)
	//	current = &dic->wordLists[alter_index];

	return WLAddWord(current, word);
}
int DicRemoveWord(Dictionary* dic, char* word)
{
	WordList* current;
	int wanted_index = tolower(word[0]) - 'a';
	int alter_index = 26;

	current = &dic->wordLists[wanted_index];
	if (WLIsExist(current, word) == FALSE)
		current = &dic->wordLists[alter_index];

	return WLRemoveWord(current, word);
}

int WLInit(WordList* list)
{
	memset(list, 0, sizeof(*list));
	list->head.next = &list->head;
	list->head.prev = &list->head;
	return TRUE;
}

static WordListNode* WLFindWord(WordList* list, char* word)
{
	WordListNode* head = &list->head;
	WordListNode* curr = head->next;

	while (curr != head)
	{
		if (strcmp(word, curr->word) == 0)
			return curr;
		curr = curr->next;
	}
	return NULL;
}

int WLIsExist(WordList* list, char* word)
{
	WordListNode* findNode = WLFindWord(list, word);
	if (findNode != NULL)
		return TRUE;
	else
		return FALSE;
}

int WLAddWord(WordList* list, char* word)
{
	int wordLen;
	int ret = FALSE;
	WordListNode* newNode;
	WordListNode* head;
	WordListNode* curr;
	WordListNode* before;
	WordListNode* after;

	if (WLIsExist(list, word))
		return FALSE;

	wordLen = strlen(word);

	newNode = (WordListNode*)malloc(sizeof(WordListNode));
	memset(newNode, 0, sizeof(*newNode));
	newNode->word = (char*)malloc(wordLen + 1);
	memcpy(newNode->word, word, wordLen + 1);

	head = &list->head;
	curr = head;
	while (curr->next != head)
	{
		curr = curr->next;
	}
	before = curr;
	after = curr->next;
	newNode->prev = before;
	before->next = newNode;

	newNode->next = after;
	after->prev = newNode;

	list->count++;
	return TRUE;
}

int WLRemoveWord(WordList* list, char* word)
{
	WordListNode* before;
	WordListNode* after;
	WordListNode* findNode = WLFindWord(list, word);
	if (findNode == NULL)
		return FALSE;
	before = findNode->prev;
	after = findNode->next;

	before->next = after;
	after->prev = before;

	free(findNode->word);
	free(findNode);
	list->count--;
	return TRUE;
}

int DicPrintWord(Dictionary* dic)
{
	WordList* current;
	int i;
	for (i = 0; i < 26; i++)		 // the others에 저장은 하지만 출력은 안함
	{
		current = &dic->wordLists[i];
		WLPrintWord(current, i);
	}
	return TRUE;
}

int WLPrintWord(WordList* list, int num)
{
	WordListNode* head = &list->head;
	WordListNode* current = head->next;
	/*if (num == 26)
	printf("the others :");
	else*/
	printf("    %c	:", num + 97);
	while (current != head)
	{
		printf(" -> %s", current->word);
		current = current->next;

	}
	printf("\n");
	return TRUE;
}


