#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define MAX_LENGTH 1080
#define WORD_LENGTH 30

typedef struct NODE{

  char word[WORD_LENGTH];
  struct NODE *link;
}NODE;

NODE *insertNode(NODE *current, char temp[WORD_LENGTH]);
void put_words(NODE *root, char temp[WORD_LENGTH]);
void printall(NODE* root);
int get_length(NODE* root);
NODE *Search(NODE *root, char temp[WORD_LENGTH]);
void deleteNode(NODE *root, char temp[WORD_LENGTH]);
void devide_word(char str[MAX_LENGTH],char r_word[WORD_LENGTH]);
int store_binary(NODE *root);
int read_binary(void);

int main(void){

   FILE *pFile;

   char str[MAX_LENGTH][MAX_LENGTH];
   char word[WORD_LENGTH][WORD_LENGTH];
   
   NODE b[26];
   NODE voca[26];
   NODE restore[26];

   char temp[30];
   char temp2[30], temp3[30];
   int flag=0, count=0 ;

   for(int i=0; i<=25; i++)
   {
     b[i].word[0]=i+97;
     b[i].link=NULL;
   }
   for(int i=0; i<=25;i++)
   {
     voca[i].word[0]=i+97;
     voca[i].link=NULL;
   }
//텍스트파일에서 읽어오기-사전
   pFile=fopen("sample.txt","r");

   if(pFile != NULL)
   { 
     while(!feof(pFile))
     { 
       fgets(str[count],sizeof(str[0]),pFile);
       for(int i=0; i<60; i++)
        {
         devide_word(str[count], word[i]);
	 put_words(&b,word[i]);
        }
       count++;
     }
     fclose(pFile);
   }
   else{
     
     printf("----FILE ERROR----\n");
     return 0;
   }
//기능 선택하기 

   while(1){
   printf("\n기능을 선택하세요\n");
   printf("[1]Search [2]Insert [3]Delete\n");
   printf("[4]store in binary [5]restore \n");
   printf("[6]Program Quit\n");
   scanf("%d",&flag);

   switch(flag)
     {
     case 1: printf("사전에서 찾을 단어를 입력하세요: ");
             scanf("%s",temp2);
	     //Search(&b, temp2);
	     if(Search(&b, temp2)!=NULL)
	     {
                put_words(&voca,temp2);
		printf("검색한 단어를 저장했습니다!\n");
	     }
	     break;

     case 2: printf("단어장에 추가할 단어를 입력하세요: ");
             scanf("%s", temp);
	     put_words(&voca,temp);
	     break;
             
     case 3: printf("단어장에서 지울 단어를 입력하세요: ");
             scanf("%s", temp3);
	     if(Search(&voca,temp3)==NULL) printf("지울 단어가 없습니다!\n");
	     else deleteNode(&voca,temp3);
	     break;

     case 4: printf("voca파일을 binary 형태로 저장합니다.\n");
             printall(&voca);
	     store_binary(&voca);
	     break;

     case 5: printf("binary file을 다시 복구합니다!\n");
             read_binary();
	     break;
     
     case 6: printf("프로그램을 종료합니다!\n");
             return 0;

     default : printf("잘못 입력하셨습니다.\n");
               printf("프로그램을 종료합니다.\n");
               return 0;
     }
   }
 
}

int read_binary(void);//-1을 반환하면 에러
int store_binary(NODE*root);//-1을 반환하면 에러


int store_binary(NODE *root)
{
  NODE *t_node=(NODE*)malloc(sizeof(NODE));
  FILE *fp=NULL;
  size_t size, count;
  char end='\n';
  int num;
  

  fp=fopen("voca.txt","wb");
  if(fp==NULL)
  {
    fprintf(stderr,"voca.txt파일을 열 수 없습니다.\n");
    return -1;
  }
   size=sizeof(char);
  for(int i=0; i<26; i++)
  {
    t_node=&root[i];
    num=get_length(t_node);
    //여기가 문제
    
    for(int j=0; j<num;j++)
    {

      if(j==0) t_node=t_node->link;
      else
      {
       count=strlen(t_node->word)/size;
       fwrite(t_node->word,size,count,fp);
       fwrite(&end,size,1,fp);
       t_node=t_node->link;
      }
    }
  }
 fclose(fp);
 return 0;
}

int read_binary(void)
{
  NODE restore[26];
  FILE *fp;

  char buffer;
  char temp[30]="";
  int count=0;
  
  for(int i=0; i<26; i++)
  {
    restore[i].word[0]=i+97;
    restore[i].link=NULL;
  }
 
  fp= fopen("voca.txt","rb");
  
  if(fp==NULL)
  {   
    fprintf(stderr,"voca.txt파일을 열 수 없습니다.\n");
    return -1;
  }

  while(!feof(fp))
  { 
      for (int i=0; i<30; i++)
      {

        fread(&buffer, sizeof(char),1,fp);
        if('a'<=buffer&&buffer<='z'||'A'<=buffer&&buffer<='Z')
	{
	  temp[count]=buffer;
	  count++;
	}
        else if(buffer=='\n')
	{ 
	 
	  put_words(&restore,temp);
	  count =0;
	  for(int j=0; j<30;j++)  temp[j]='\0';
	  break;
        }
      }
  }
  fclose(fp);
   printall(&restore);
  return 0;

}


int get_length(NODE *root)
{
  NODE *p;
  int length=0;
  p=root;
  while(p)
  {
    length++;
    p=p->link;
  }
  return length;
}

void put_words(NODE *root, char temp[30])
{ 

  NODE *t_node=(NODE*)malloc(sizeof(NODE));
  int num=0;
  
  for(int i=0;i<=25; i++)
  { 
    if(root[i].word[0]==temp[0]||(root[i].word[0]-32)==temp[0])
    { 
       num=get_length(&root[i]);
       t_node=&root[i];
       for(int j=1; j<num;j++)
       {t_node=t_node->link; }
       insertNode(t_node,temp);
      
      }
    }
  }

void printall(NODE *root)
{ 
  NODE *t_node;
  for(int i=0; i<26; i++)
  { 
    t_node=&root[i];
    while(t_node!=NULL)
    { 
      printf("[%d]-%s\n",i, t_node->word);
      t_node = t_node->link;
     }
  } 
}

NODE *insertNode(NODE * current, char temp[30])
{
  NODE * newNode=(NODE*)malloc(sizeof(NODE));
  strcpy(newNode->word, temp);
  newNode->link=NULL;

  current->link=newNode;

  return newNode;
}


NODE *Search(NODE *root, char temp[30])
{
  NODE *search=(NODE*)malloc(sizeof(NODE));
   for(int i=0; i<26; i++)
   { 
   if(root[i].word[0]==temp[0]||(root[i].word[0]-32)==temp[0])
     {
       search=&root[i];
       while((search!=NULL)&&(strcmp(search->word, temp)!=0))
        search=search->link;
        if(search==NULL)
	{printf("error!\n");
	 return (NULL);}
	else 
	{printf("단어가 있습니다=)\n");
	return search;}
     }
   
   }
   printf("단어가 없습니다.=(\n");
   return (NULL);

}

void deleteNode(NODE *root, char temp[30])
{
  NODE *t_node=(NODE*)malloc(sizeof(NODE));
  NODE *d_node=(NODE*)malloc(sizeof(NODE));

  for(int i=25; i>=0; i--)
  { 
    t_node=&root[i];
    while(t_node->link!=NULL)
      {
        if(strcmp(t_node->link->word, temp)==0)
        {
          d_node=t_node->link;
	  t_node->link=t_node->link->link;
	  free(d_node);
	  printf("단어를 지원습니다!\n");

	  return ;
        }
	
      t_node=t_node->link;
    }
   }
}

void devide_word(char str[MAX_LENGTH],char r_word[WORD_LENGTH])
{
  static int start=0;
  static int end=0;
  char word[WORD_LENGTH];

  for(end=start; end< start+WORD_LENGTH; end++)
  {
    if(str[end]=='\0')
    {
      int temp=0;
      for(int i=start; i<end; i++){
        if(('a'<=str[i]&&str[i]<='z')||('A'<=str[i]&&str[i]<='Z'))
	 {word[temp]=str[i];
	  temp++;}
      }
      word[temp]='\0';
      start=0;
      end=0;
      strcpy(r_word, word);
      return ;
    }

    if(str[end]==' '){
      int temp=0;
      for(int i=start; i<end; i++){
        if(('a'<=str[i]&&str[i]<='z')||('A'<=str[i]&&str[i]<='Z'))
        {word[temp]=str[i];
	 temp++;}
      }
      word[temp]='\0';
      start=++end;
      strcpy(r_word, word);
      return ;
    }
  }
}

