#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <unistd.h>
/*
	LinkedList (연결리스트) 의 데이터 및 기능을 정의하는 헤더파일
*/

#ifndef				_LINKED_LIST_H_
#define			_LINKED_LIST_H_

#define TRUE						1
#define FALSE						0
#define WORD_LENGTH		30 // 하나의 단어는 30글이자 이하라고 가정한다. 숫자를 변경하여 더 크게 잡을수도 있다.

/*
	노드의 정의	
*/
typedef struct _node
{
	char data[WORD_LENGTH]; // 최대 30글자짜리의 단어
	struct _node* next; // 다음 노드의 포인터
} Node;


/*
	연결리스트의 정의	
*/

typedef struct _linkedList
{
	Node* head; // 시작점 노드
	Node* cur; // 현재 노드
	Node* before; // 이전 노드
	int numOfData; // 노드의 갯수
}LinkedList;

typedef LinkedList List;

/* 연결리스트를 초기화한다. */
void ListInit(List* plist);
/* 연결리스트에 데이터를 추가한다. */
void LInsert(List* plist, char* data);
/* 연결리스트의 첫번째 노드의 데이터를 구해온다. (단어) */
int LFirst(List* plist, char* data);
/* 연결리스트의 현재 노드 다음 노드의 데이터를 구해온다. (단어) */
int LNext(List* plist, char* data);
/* 연결리스트의 현재 노드를 삭제한다.*/
void LRemove(List* plist);
/* 연결리스트의 현재 저장된 노드 갯수를 구해온다. */
int LColunt(List* plist);
/* 연결리스트에서 특정 데이터를 찾아 삭제한다. */
int SearchAndDelete(List* plist, char* data);
/* 연결리스트에서 특정 데이터를 찾아온다. */
int SearchData(List* plist, char* data);
/* 연결리스트를 출력한다. */
void PrintList(List* plist);

#endif

/* 파일에서 단어를 읽어와 연결리스트로 만든다. */
void ReadFile(List* plist);
/* 단어장 단어를 읽어와 연결리스트로 만든다. */
void ReadWordFile(List* plist);
/* 사전에서 단어를 검색하고 단어장에 저장한다. */
void SearchFromDicAndSave(List* DicList, List* WordList);
/* ACII모드로 단어장을 저장시킨다. */
void SaveFileToASCII(List* WordList);
/* Binary모드로 단어장을 저장시킨다. */
void SaveFileToBinary(List* WordList);
/* 단어장에서 단어를 찾는다. */
void SearchWord(List* plist);
/* 단어장에 단어를 추가한다. */
void InsertWord(List* plist);
/* 단어장에서 단어를 지운다. */
void DeleteWord(List* plist);
/* 현재 단어장의 정보를 출력한다. */
void PrintTest(List* plist);

int main()
{
	int menu, i;
	List DicList[26];			 // 사전에서 읽어온 데이터를 저장할 리스트 (알파벳 26개)
	List WordList[26];		 // 단어장에 저장할 리스트 (알파벳 26개)

	for(i = 0; i < 26; i++)
	{
		ListInit(DicList+i);			// 리스트를 모두 초기화한다.
		ListInit(WordList+i);		// 리스트를 모두 초기화한다.

	}
	

	ReadFile(DicList);					// 리스트배열 전체를 보내 파일을 읽어 초기화 한다. (사전)
	ReadWordFile(WordList);		// 리스트배열 전체를 보내 파일을 읽어 초기화 한다. (단어장)

	do
	{		
		// 메뉴를 출력한다. 7번이 아니면 계속 반복한다.
		printf("- Dictionary Menu -\n");		
		printf("    [1] Search\n");

		printf("- Vocabulary Menu -\n");		
		printf("    [2] Search\n");
		printf("    [3] Insert\n");
		printf("    [4] Delete\n");
		printf("    [5] Store in ASCII\n");
		printf("    [6] Store in Binary\n");
		printf("    [7] Quit\n");
		printf(": ");
		scanf("%d", &menu);

		switch(menu)
		{
			case 1: // 사전에서 찾아 단어장에 저장하기
				SearchFromDicAndSave(DicList, WordList);
				break;
			case 2: // 단어장에서 찾기				
				SearchWord(WordList);				
				break;
			case 3: // 단어장에 단어 추가
				InsertWord(WordList);
				break;
			case 4: // 삭제
				DeleteWord(WordList);
				break;
			case 5: // 아스키모드 저장				
				SaveFileToASCII(WordList);
				break;
			case 6: // 이진모드 저장				
				SaveFileToBinary(WordList);
				break;
			case 7: // 종료
				break;
			case -1:
				PrintTest(WordList);		// 테스트용. 기능상에서는 보이지 않지만, -1을 입력하면 모든 링크드리스트를 출력한다. (단어장)
				break;
			case -2:
				PrintTest(DicList);			// 테스트용. 기능상에서는 보이지 않지만, -1을 입력하면 모든 링크드리스트를 출력한다. (사전)
				break;
			default:							// 잘못 입력한 경우
				printf("Input Error.\n");
				break;
		}

		printf("\n");
	}while(menu != 7);

	return 0;
}


/*
	사전(sample.txt) 파일을 읽어 연결리스트에에 저장한다.
*/
void ReadFile(List* plist)
{
	FILE * fp = NULL; // 파일포인터
	char buffer[1024]; // 한줄씩 읽어오되 최대 1024만큼 읽어온다.
	char* token = " \n"; // 토큰, 공백과 엔터를 기준으로 분리한다.
	char* tokResult; // pointer that save seperate word token ;
	int i;
	int offset;
	int size;


	fp = fopen("sample.txt", "r"); // Read File. If file directory of file name is changed, change this code.


	if(fp == NULL) // 파일 오픈에러가 발생하면 종료한다.
	{
		printf("FILE OPEN ERROR !! \n");
		exit(0);
	}

	while(!feof(fp)) // 파일의 끝에 도달하지 않는다면 무한 반복한다.
	{
		fgets(buffer, 1024, fp); // 최대 1024만큼 한줄을 읽어온다.

		tokResult = strtok(buffer, token); // 토큰으로 분리시킨다.

		while(tokResult != NULL) // NULL이 아니라면 분리한 결과가 계속 존재한다는 의미이다.
		{
			int IsExist = FALSE;
			// 여기서 링크드리스트에 저장을 시켜야 한다.
			/*
				케이스 분류.

				1. 알파벳이아닌 문자가 들어있다.
				2. 시작문자가 대문자이다.
				3. 시작문제가 소문자이다.
			*/

			// 케이스 1
			size = strlen(tokResult);
			for(i = 0; i < size ; i++)
			{
				if(!isalpha(tokResult[i])) // 알파벳이 아닌문자가 들어있다. 버린다.
					break;
			}

			if(i == strlen(tokResult)) // 모두 알파벳이다.
			{
				// 케이스 2/3
				if(isupper(tokResult[0])) // 시작문자가 대문자. 대문 A의 아스키코드는 65이다.
					offset = tokResult[0] - 65;
				else // 시작문자가 소문자
					offset = tokResult[0] - 97; // 소문자 a의 아스키코드는 97이다.

				// 이제 저장을 시작한다.

				/*
					1. 먼저 파싱된 단어가 이미 존재하는지를 검사해야 한다. 그렇다면 패스한다.
					2. 이미 존재하지 않는 경우, 알파벳링크드리스트에 넣는다.
				*/

				// 1. 찾는다.
				if(SearchData(plist + offset, tokResult)) // 단어를 해당 알파벳 링크드리스트에서 찾은경우
				{
					IsExist = TRUE;
				}
				else // 단어를 해당 알파벳 링크드리스트에서 못찾았다.
				{
					// 여기까지 왔다면 해당 알파벳 링크드리스트에 해당 데이터가 없는 경우이다. 
					IsExist = FALSE;
				}

				// 찾아봤는데 존재하지 않는다. 단어를 추가해야한다.
				if(IsExist == FALSE)
				{
					// 해당 알파벳 연결리스트에 데이터를 저장한다.
					LInsert(plist + offset, tokResult); 
				}
			}

			tokResult = strtok(NULL, token); // 다음 분리를 시작한다.
		}		
	}

	fclose(fp); // 파일을 닫는다.
}


/*
	단어장(voc.txt) 파일을 읽어 연결리스트에 저장한다.
*/
void ReadWordFile(List* plist)
{
	char buffer[WORD_LENGTH] ={0};		// 단어의 최대길이는 30글자 이하라고 가정한다.
	char One;												// 1개의 문자를 읽어 저장할 변수
	int i, IsFileEnd = 1;								// 읽어낸 갯수가 1이 아니라면 파일이 끝났음을 의미한다.
	int Offset;
	FILE* fp = NULL;
	
	// 우선 파일이 존재하는지 부터 체크한다.
	if (access("voc.txt", F_OK) != 0) // 파일이 존재하면 0을 리턴, 존재하지 않으면 1을 리턴한다.
	{
		// 파일이 존재하지 않으므로 생성할 리스트가 없다.
		return;		
	}
	else // 파일이 존재한다. 리스트를 만든다.
	{
		fp = fopen("voc.txt", "rb"); // 이진파일 읽기모드

		while(1) // 파일의 끝이 나타날때까지 계속 읽어야한다.
		{
			for(i = 0; i < WORD_LENGTH; i++)
			{
				IsFileEnd = fread(&One, sizeof(char), 1, fp); // 1글자를 읽는다.

				if(IsFileEnd != 1) // 만약 1이라면 무엇인가 글자를 읽은것이며, 0이라면 더이상 읽을것이 없음을 나타낸다.
					break;

				if(One != '\n') // 방금 읽어낸 문자가 줄바꿈 문자가 아니라면 단어가 진행중이므로 buffer에 하나씩 집어넣는다.
					buffer[i] = One;					
				else // 만약 읽어낸 문자가 줄바꿈 문자라면 하나의 단어가 끝났음을 의미하므로 for문을 빠져나간다.
				{					
					break;
				}			
			}
									
			// 읽어낸 단어를 연결리스트에 추가한다.
			if(isupper(buffer[0])) // 시작문자가 대문자. 대문 A의 아스키코드는 65이다.
				Offset = buffer[0] - 65;
			else // 시작문자가 소문자
				Offset = buffer[0] - 97; // 소문자 a의 아스키코드는 97이다.
			LInsert(plist + Offset, buffer); // 추가


			memset(buffer, 0, WORD_LENGTH); // buffer를 초기화

			if(IsFileEnd != 1) // 만약 파일의 끝이라면 더이상 반복할 필요가 없으므로 while loop를 빠져나간다.
				break;
		
		}
		
	}

	fclose(fp);
}

/*
	사전에서 단어를 검색한후, 단어장에 저장한다.
*/
void SearchFromDicAndSave(List* DicList, List* WordList)
{
	char inputWord[WORD_LENGTH];
	int i;
	int offset;
	int size;

	// a 는 97 
	// A 는 65

	// 추가할 단어를 입력받는다.
	printf("Type Word To Search : ");
	scanf("%s", inputWord);

	// 알파벳이 아닌문자가 들어있다면 다시 입력받는다.
	size = strlen(inputWord);
	for(i = 0; i < size; i++)
	{
		if(!isalpha(inputWord[i]))
		{
			printf("Only Alpabhet Is Accepted.\n");
			return;
		}
	}

	// 제대로 입력했다면
	if(isupper(inputWord[0])) // 첫번째 시작 문자가 대문자라면
		offset = inputWord[0] - 65;
	else
		offset = inputWord[0] - 97;

	if(SearchData(DicList + offset, inputWord)) // 사전에 단어가 존재하는지 확인한다.
	{
		// 단어장에 저장한다.
		if(SearchData(WordList + offset, inputWord)) // 단어장에 이미 추가된 단어인지 조사한다.
		{
			printf("-> Word You Search Is Already Existed.\n");
			return;
		}
		else
		{
			LInsert(WordList + offset, inputWord);
			printf("%s Word Is Successfully Added.\n", inputWord);
		}
	}
	else // 사전에 단어가 존재하지 않는다.
	{		
		printf("%s Word You Search Is Not Existed In Dictionary.\n", inputWord);
	}
}


/* 
	ACII모드로 단어장을 저장시킨다. 
*/
void SaveFileToASCII(List* WordList)
{
	int i;
	FILE* fp = NULL;

	fp = fopen("vocASCII.txt", "w"); // 파일을 쓰기모드로 오픈한다.
	if(fp == NULL)
	{
		printf("FILE OPEN ERROR !!\n");
		exit(0);
	}

	// 26개의 연결리스트에 대해서 
	for(i = 0; i < 26; i++)
	{
		if((*(WordList+i)).numOfData != 0) // 해당 알파벳의 리스트가 단어 1개라도 가지고 있다면
		{
			char temp[WORD_LENGTH];

			if(LFirst(WordList+i, temp)) // 첫번째 단어를 찾아서 파일에 기록한다.
			{
				if((WordList+i)->cur->next == NULL) // 만약 다음 단어가 없다면 "->"는 필요없다.
					fprintf(fp, "%s", temp);
				else // 다음단어가 존재한다면 "->"를 출력한다.
					fprintf(fp, "%s -> ", temp);
			}
	
			while(LNext(WordList+i, temp)) // 다음단어가 존재하지 않을때까지 계속 해서 파일에 출력한다.
			{		
				if((WordList+i)->cur->next == NULL)
					fprintf(fp, "%s", temp);
				else
					fprintf(fp, "%s -> ", temp);
			}

			fprintf(fp, "\n"); // 하나의 알파벳이 끝나면 다음줄로 내려간다.
		}		
	}	

	printf("vocASCII File Is Save as ASCII Mode .\n");

	fclose(fp);
}


/* 
	Binary모드로 단어장을 저장시킨다. 
*/
void SaveFileToBinary(List* WordList)
{
	FILE* fp = NULL;
	int i;

	fp = fopen("voc.txt", "wb"); // 이진쓰기 모드로 파일을 오픈한다.
	if(fp == NULL)
	{
		printf("FILE OPEN ERROR !!\n");
		exit(0);
	}

	for(i = 0; i < 26; i++) // 26개의 알파벳 연결리스트에 대해
	{
		if((*(WordList+i)).numOfData != 0) // 해당 알파벳에 단어가 존재한다면
		{
			char temp[WORD_LENGTH];

			if(LFirst(WordList+i, temp))
			{
				fwrite(temp, strlen(temp), 1, fp); // 2진모드로 단어를 저장한다. (temp에 있는 단어를 temp의 길이만큼 1번 저장한다.)
				fwrite("\n", 1, 1, fp); // 그리고 줄바꿈 문자를 넣어서 추후 이 파일을 읽어올때 구분자로 사용한다.
			}
	
			while(LNext(WordList+i, temp)) // 해당 알파벳의 단어가 존재하지 않을때까지 같은 방법으로 계속 저장한다.
			{		
				fwrite(temp, strlen(temp), 1, fp);	
				fwrite("\n", 1, 1, fp);
			}
		}		

		// 여기까지 오면 하나의 알파벳 리스트에 있는 단어를 모두 저장한것으로 위로 올라가서 다음 알파벳에 대한 반복을 시작한다.
	}	

	printf("voc.txt File Is Saved As Binary Mode.\n");
	

	fclose(fp);
}


/*
	연결리스트에서 단어를 찾는다.
*/
void SearchWord(List* plist)
{
	char inputWord[WORD_LENGTH];
	int i;
	int offset;
	int size;

	// a 는 97 
	// A 는 65

	// 찾을 단어를 입력받는다.
	printf("Type Word To Search : ");
	scanf("%s", inputWord);

	// 알파벳이 아닌단어는 입력받아도 없으므로 에러처리하고 다시 입력하도록 유도한다.
	size = strlen(inputWord);
	for(i = 0; i < size; i++)
	{
		if(!isalpha(inputWord[i]))
		{
			printf("Only Alpabhet Is Accepted.\n");
			return;
		}
	}

	if(isupper(inputWord[0])) // 첫번째 시작 문자가 대문자라면
		offset = inputWord[0] - 65;
	else
		offset = inputWord[0] - 97;
	
	if(SearchData(plist + offset, inputWord)) // 단어를 해당 알파벳 링크드리스트에서 찾은경우
	{
		printf("-> Word You Search Is Already Existed.\n");		
	}
	else // 단어를 해당 알파벳 링크드리스트에서 못찾았다.
	{
		printf("-> Word You Search Is Already Existed.\n");		
	}

	
}

/*
	연결리스트에 단어를 추가한다.
*/
void InsertWord(List* plist)
{
	char inputWord[WORD_LENGTH];
	int i;
	int offset;
	int size;

	// a 는 97 
	// A 는 65

	// 추가할 단어를 입력받는다.
	printf("Type Word To Add  : ");
	scanf("%s", inputWord);

	// 알파벳이 아닌문자가 들어있다면 다시 입력받는다.
	size = strlen(inputWord);
	for(i = 0; i < size; i++)
	{
		if(!isalpha(inputWord[i]))
		{
			printf("Only Alpabhet Is Accepted.\n");
			return;
		}
	}

	if(isupper(inputWord[0])) // 첫번째 시작 문자가 대문자라면
		offset = inputWord[0] - 65;
	else
		offset = inputWord[0] - 97;

	if(SearchData(plist + offset, inputWord)) // 단어를 해당 알파벳 링크드리스트에서 찾은경우
	{
		printf("-> Word You Search Is Already Existed.\n");
		return;
	}
	else // 단어를 해당 알파벳 링크드리스트에서 못찾았다.
	{
		LInsert(plist + offset, inputWord);
		printf("%s Word Is Successfully Added.\n", inputWord);
	}
}

/*
	연결리스트에서 단어를 제거한다.
*/
void DeleteWord(List* plist)
{
	char inputWord[WORD_LENGTH];
	int i;
	int offset;
	int size;

	// a 는 97 
	// A 는 65

	// 삭제할 단어를 입력받는다.
	printf("Type Word To Delete : ");
	scanf("%s", inputWord);

	// 알파벳이 아닌 문자가 들어 있다면 에러처리한다.
	size = strlen(inputWord);
	for(i = 0; i < size; i++)
	{
		if(!isalpha(inputWord[i]))
		{
			printf("Only Alpabhet Is Accepted.\n");
			return;
		}
	}

	// 찾아서 삭제한다.
	if(isupper(inputWord[0])) // 첫번째 시작 문자가 대문자라면
		offset = inputWord[0] - 65;
	else
		offset = inputWord[0] - 97;
	
	if(SearchAndDelete(plist + offset, inputWord)) // 단어를 해당 알파벳 링크드리스트에서 찾은경우
	{
		printf("-> Word Is Successfully Deleted.\n");		
	}
	else // 단어를 해당 알파벳 링크드리스트에서 못찾았다.
	{
		printf("-> Word To Delete Is Not Existed In Dictionary.\n");
	}
}

/*
	연결리스트의 단어들을 출력한다.
*/
void PrintTest(List* plist)
{
	int i;
	for(i = 0; i < 26; i++)
	{
		if((*(plist+i)).numOfData != 0)
		{
			PrintList(plist + i);
			printf("\n");
		}
		
	}	
}
/*
	연결리스트를 초기화한다.
*/
void ListInit(List* plist)
{
	// 새 노드를 만들어 시작점으로 등록한다. (동적메모리할당)
	plist->head = (Node*) malloc(sizeof(Node));
	plist->head->next = NULL;
	plist->numOfData = 0;
}

/*
	연결리스트에 노드를 추가한다.
*/
void LInsert(List* plist, char* data)
{
	// 새로운 노드 생성
	Node* newNode = (Node*) malloc(sizeof(Node)); 

	// 첫번째 노드를 현재 노드로 설정한다. (이후 아래작업)
	Node* cur = plist->head;

	// 새 노드에 데이터를 추가한다.
	strcpy(newNode->data, data);

	// 마지막 노드의 끝에 연결하기 위해 끝까지 찾아간다.
	while(cur->next != NULL)	
		cur = cur->next; 
	
	// 현재노드를 새 노드로 정의한다.
	// 마지막 노드의 뒤에는 NULL을 넣어서 끝임을 저장시킨다.
	cur->next = newNode;
	newNode->next = NULL;

	// 리스트의 갯수를 하나 증가시킨다.
	(plist->numOfData)++;
}

/*
	연결리스트에서 첫번째 데이터를 구해온다.
*/
int LFirst(List* plist, char* data)
{
	// 비어 있다면 데이터는 존재하지 않으므로 리턴한다.
	if(plist->head->next == NULL)
		return FALSE;

	// 첫번째 노드가 존재하면 data 포인터에 리스트의 첫번째 데이터를 저장시킨다.
	plist->before = plist->head;
	plist->cur = plist->head->next;
	strcpy(data, plist->cur->data);
	return TRUE;
}

/*
	연결리스트에서 현재 다음 노드의 데이터르 구해온다.
*/
 int LNext(List* plist, char* data)
{
	// 다음이 NULL이면 데이터가 존재하지 않으므로 리턴한다.
	if(plist->cur->next == NULL)
		return FALSE;

	// 현재노드의 다음 데이터를 찾아 데이터를 data에 저장시킨다.
	plist->before = plist->cur;
	plist->cur = plist->cur->next;	
	strcpy(data, plist->cur->data);

	return TRUE;
}

 /*
	연결리스트에서 현재 노드를 삭제한다.
 */
void LRemove(List* plist)
{
	// 현재 노드를 구해온다.
	Node* rpos = plist->cur;
	/*char temp[WORD_LENGTH];
	strcpy(temp, rpos->data);*/

	// 이전 노드의 다음을 현재노드의 다음으로 연결시킨다. 
	// (현재노드가 중간에서 삭제되므로 이를 제외하고 양옆의 노드들을 연결시켜야한다.)
	plist->before->next = plist->cur->next;
	plist->cur = plist->before;

	// 삭제할 노드의 메모리를 해제시킨다.
	free(rpos);

	(plist->numOfData)--;
}

/*
	연결리스트의 갯수를 리턴한다.
*/
int LColunt(List* plist)
{
	return plist->numOfData;
}

/*
	연결리스트에서 원하는 데이터가 들어 있는 노드를 찾아 삭제한다.
*/
int SearchAndDelete(List* plist, char* data)
{
	char temp[WORD_LENGTH];
	
	// 첫번째 데이터를 가져와서 존재한다면
	if(LFirst(plist, temp))
	{
		// 찾고자 하는 데이터와 비교해서 같다면
		if(strcmp(temp, data) == 0)
		{
			// 삭제하고 리턴한다.
			LRemove(plist);
			return TRUE;
		}

		// 만약 첫번째 노드가 찾고자 하는 데이터가 아니었다면,
		while(LNext(plist, temp))
		{
			// 같은게 나올때까지 찾다가
			if(strcmp(temp, data) == 0)
			{
				// 찾으면 삭제하고 리턴한다.
				LRemove(plist);
				return TRUE;
			}
		}
	}

	// 끝까지 못찾았다면 없다는 의미로 FALSE를 리턴시킨다.
	return FALSE;
}

/*
	연결리스트에서 원하는 데이터를 찾는다.
*/
int SearchData(List* plist, char* data)
{
	char temp[WORD_LENGTH];	
	
	// 첫번째 데이터가 존재하면
	if(LFirst(plist, temp))
	{
		// 찾고자 하는 데이터와 일치하면
		if(strcmp(temp, data) == 0)
		{
			// 데이터는 존재하므로 TRUE를 리턴한다.
			return TRUE;
		}

		// 첫번째 데이터가 찾고자 하는 데이터가 아니라면
		while(LNext(plist, temp))
		{
			// 찾을때까지 반복하다 찾으면
			if(strcmp(temp, data) == 0)
			{
				// 찾으면 TRUE를 리턴한다.
				return TRUE;
			}
		}
	}

	// 못찾았다.
	return FALSE;
}

/*
	연결리스트의 데이터들을 -> 로 연결된 형태로 보여준다.
*/
void PrintList(List* plist)
{
	char temp[WORD_LENGTH];

	if(LFirst(plist, temp))
	{
		printf("%s -> ", temp);
	}
	
	while(LNext(plist, temp))
	{		
		if(plist->cur->next == NULL)
			printf("%s", temp);
		else
			printf("%s -> ", temp);
	}
}