﻿#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#define MAX 26


typedef struct _node{
	char value[40];
	struct _node* next;
}node;

typedef node* nptr;

typedef struct _list{
	int count;
	node* head;
}list;

typedef list* lptr;

list* voca;


void addlast(list* lptr,char value[]);
void init(list* lptr);
//
void search(list* lptr);
void insert(list* lptr,char word[]);
void delete(list* lptr);
//
void voca_search();
void voca_insert(char word[]);
void voca_delete();

int getsize(node* head);
void printall(list* lptr);
void store_in_ASCII();
void store_in_binary();

char *replaceAll(char*s,const char*olds,const char *news);

int main(){
	int buf_size=400;
	char buf[400];
	FILE *f=fopen("sample.txt","r+");
	FILE *ff=fopen("BINARY.txt","rb");
	voca=(list*)malloc(sizeof(list));
	if(ff){
		while(fread(buf,sizeof(char),sizeof(char)*40,ff)){
			printf("%s\n", buf);
			addlast(voca,buf);
		}
	}
	char *word;
	
	
	word=(char *)malloc(sizeof(char)*30);
	list *_list=(list *)malloc(sizeof(list)*MAX);
	for(int i=0;i<MAX;i++){
		(_list+i)->head=(node *)malloc(sizeof(node));
		(_list+i)->head->next = NULL;
	}
	voca -> head = (nptr)malloc(sizeof(node));
	voca -> head -> next = NULL;

	while(fgets(buf,buf_size,f))
	{
		char *buf2 = (char*)malloc(sizeof(char)*30);
		buf2=replaceAll(buf,",","");
		buf2=replaceAll(buf2,".","");
		buf2=replaceAll(buf2,"'","");
		buf2=replaceAll(buf2,"\"","");
		buf2=replaceAll(buf2,"!","");
		buf2=replaceAll(buf2,"?","");
		word=strtok(buf2, " \n");
		
		//printf("1\n");
		while(word!=NULL)
		{
			word=strlwr(word);
			if(isalpha(word[0]) != 0){
				insert(_list,word);
			}
			word=strtok(NULL," \n");	
		}
	}
	fclose(f);
	fclose(ff);

	while(1){
		int menu;
		printf("메뉴를 고르시오.\n");
		printf("[1]Search [2]Insert [3]Delete\n[4]단어장Search [5]단어장 Insert [6]단어장 Delete\n[7]store in ASCII [8]store in binary [9]QUit\n");
		scanf("%d",&menu);
		if(menu==1){
			search(_list);
		}else if(menu==2){
			char* insertword=(char *)malloc(sizeof(char)*30);
			printf("사전에 추가하고 싶은 단어를 입력하세요.\n");
			scanf("%s",insertword);
			insertword=strlwr(insertword);
			insert(_list,insertword);
			free(insertword);
		}else if(menu==3){
			delete(_list);
		}else if(menu==4){
			voca_search();
		}else if(menu==5){
			char* _insertword=(char *)malloc(sizeof(char)*30);
			printf("단어장에 추가하고싶은 단어를 입력하세요.\n");
			scanf("%s",_insertword);
			_insertword=strlwr(_insertword);
			voca_insert(_insertword);
			free(_insertword);
		}else if(menu==6){
			voca_delete();
		}else if(menu==7){
			store_in_ASCII();
		}else if(menu==8){
			store_in_binary();
		}else if(menu==9){
			break;
		}
	}
}	
	

void init(list* lptr){
	lptr->count=0;
	lptr->head=NULL;
}
void insert(list* lptr,char word[]){
	int index = word[0] - 97;
	
	addlast(lptr+index,word);
}
void voca_insert(char word[]){
	addlast(voca,word);
}
node* search_node(list* lptr,char value[]){
	node* last=lptr->head;
	while(last->next!=NULL)
	{
		if(strcmp(last->value,value)==0)
			return last;
		last=last->next;
	}
	if(strcmp(last->value,value)==0)
		return last;
	return NULL;
}
void delete(list* lptr){
	char* deleteword=(char *)malloc(sizeof(char)*30);
	printf("삭제하고싶은 단어를 입력하세요.\n");
	scanf("%s",deleteword);
	deleteword=strlwr(deleteword);
	int index=deleteword[0]-97;
	node* last=(lptr+index)->head;
	while(last -> next != NULL){
		if(strcmp(last->next->value, deleteword) == 0){
			node *temp = last->next;
			last -> next = last -> next -> next;
			free(temp);
			return ;
		}
		last = last -> next;
	}
	if(strcmp(last->value,deleteword)==0){
		free(last);
		last=NULL;
	}
	printf("삭제되었습니다.\n");
	free(deleteword);
}
void voca_delete(){
	char* deleteword=(char *)malloc(sizeof(char)*30);
	printf("단어장에서 삭제하고 싶은 단어를 입력하세요.\n");
	scanf("%s",deleteword);
	deleteword=strlwr(deleteword);
	node* last = voca->head;
	while(last->next!=NULL){
		if(strcmp(last->next->value, deleteword)==0){
			node *temp=last->next;
			last -> next = last -> next -> next;
			free(temp);
			return;
		}
		last = last ->next;
	}
	if(strcmp(last->value,deleteword)==0){
		free(last);
		last=NULL;
	}
	printf("삭제되었습니다.?\n");
	free(deleteword);
}
void search(list* lptr){
	char* findword=(char*)malloc(sizeof(char)*30);
	printf("찾을단어를 입력하세요..\n");
	scanf("%s",findword);
	findword=strlwr(findword);
	int index=findword[0]-97;
	node* n = search_node(lptr+index,findword);
	if(n==NULL){
		printf("단어가 없습니다...\n");
	}else{
		printf("단어가 있습니다..\n");
	}
	printf("%s\n", findword);
	addlast(voca,findword);
	free(findword);

}
void voca_search(){
	char* findword=(char*)malloc(sizeof(char)*30);
	printf("찾을단어를 입력하세요.\n");
	scanf("%s",findword);
	findword=strlwr(findword);
	node* n=search_node(voca,findword);
	if(n==NULL){
		printf("단어가 있습니다.\n");
	}else{
		printf("단어가 없습니다.\n");
	}
	printf("%s\n", findword);
	addlast(voca,findword);
	free(findword);	
}

void addlast(list* lptr,char value[]){
	node* last=lptr->head;
	node* newnode;
	newnode=(nptr)malloc(sizeof(node));
	strcpy(newnode->value,value);

	while(last->next!=NULL)
	{
		last=last->next;	
	}
	last->next=newnode;
}
int getsize(node* head){ 
	if(head==NULL){
		return 0;
	}
	else{
		return(1+getsize(head->next));
	}
}


char *replaceAll(char *s, const char *olds, const char *news) {
  char *result, *sr;
  size_t i, count = 0;
  size_t oldlen = strlen(olds); if (oldlen < 1) return s;
  size_t newlen = strlen(news);


  if (newlen != oldlen) {
    for (i = 0; s[i] != '\0';) {
      if (memcmp(&s[i], olds, oldlen) == 0) count++, i += oldlen;
      else i++;
    }
  } else i = strlen(s);


  result = (char *) malloc(i + 1 + count * (newlen - oldlen));
  if (result == NULL) return NULL;


  sr = result;
  while (*s) {
    if (memcmp(s, olds, oldlen) == 0) {
      memcpy(sr, news, newlen);
      sr += newlen;
      s  += oldlen;
    } else *sr++ = *s++;
  }
  *sr = '\0';

  return result;
}

void printall(list* lptr){
	int i;
	for(i=0;i<MAX;i++){
		if(i== MAX-1){
			printf("the others\n");
		} else {
			printf("<%c>", i+97);
		}
		node* temp = (lptr+i)->head;
		while(temp != NULL){
			printf("\t%s\n", temp -> value);
			temp = temp -> next;
		}
	}
}

void store_in_ASCII(){
	FILE *fp=fopen("ASCII.txt","w");
	node *temp=voca->head;
	while(temp!=NULL){
		fprintf(fp,"%s\n",temp->value);
		temp = temp->next;
	}
	fclose(fp);
}

void store_in_binary(){
	FILE *fp=fopen("BINARY.txt","wb");
	node *temp=voca->head;
	while(temp!=NULL){
		fwrite(temp->value,sizeof(char),sizeof(temp->value),fp);
		temp = temp->next;
	}
	fclose(fp);
}



