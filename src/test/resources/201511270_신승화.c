#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

typedef struct Node{
    char element[32];
    struct Node *prev;
    struct Node *next;
}NODE;

typedef int INT;

typedef struct List{
    NODE *header;
    NODE *trailer;
    
}LIST;

void addLast(LIST *list, char txtword[]){
    NODE* newNODE = (NODE*)malloc(sizeof(NODE));
    newNODE->prev = list->trailer->prev;
    newNODE->next = list->trailer;
    strcpy(newNODE->element, txtword);
    
    list->trailer->prev->next = newNODE;
    list->trailer->prev = newNODE;
}

void printList(LIST *list){
    int i;
    for (i = 0; i<27; i++){
        printf("<%c>\n", i + 65);
        NODE *temp = list[i].header->next;
        while (temp != list[i].trailer){
            printf("\t%s\n", temp->element);
            temp = temp->next;
        }
    }
    
}

int get_size(LIST *list){
    int size = 0;
    
    NODE *temp = list->header->next;
    while (temp != list->trailer){
        size++;
        temp = temp->next;
    }
    
    return size;
}

char sep[] = "_-.,!?/; \t \"\': ";

int main() {
    
    FILE *fp;
    int a=0, i;
    char word[32];
    char line[512];
    LIST list[26];
    LIST voc;
    char arr[32];
    
    for (i = 0; i < 26; i++)
    {
        list[i].header = (NODE*)malloc(sizeof(NODE));
        list[i].trailer = (NODE*)malloc(sizeof(NODE));
        list[i].header->next = list[i].trailer;
        list[i].trailer->prev = list[i].header;
        strcpy(list[i].header->element, " ");
        strcpy(list[i].trailer->element, " ");
    }
    
    voc.header = (NODE*)malloc(sizeof(NODE));
    voc.trailer = (NODE*)malloc(sizeof(NODE));
    voc.header->next = voc.trailer;
    voc.trailer->prev = voc.header;
    strcpy(voc.header->element, " ");
    strcpy(voc.trailer->element, " ");
    
    
    
    fp = fopen("//Users//shinseunghwa//Desktop//sample.txt", "r");
    
    if (fp == NULL)
    {
        printf("파일 열기 실패\n");
        exit(1);
        
    }
    
    while (1)
    {
        if (feof(fp))
            break;
        fgets(line, 512, fp);
        
        char* txtword = strtok(line, sep);
        while (txtword != NULL){
            if (isalpha(txtword[0])){
                txtword[0] = toupper(txtword[0]);
                int index = txtword[0] - 'A';
                
                addLast(&list[index], txtword);
            }
            txtword = strtok(NULL, sep);
        }
    }
    fclose(fp);
    
    FILE *fp2;
   
    
    fp2 = fopen("abcd.bin", "rb");
    
    if (fp2 != NULL){
        while (0<fread(arr, sizeof(arr), 1, fp2)){
            addLast(&voc, arr);
        }
    }
    
    fclose(fp2);
    
    while (a != 7){
        printf("기능을 선택하세요(사전)\n[1]Search \n");
        printf("기능을 선택하세요(단어장)\n[2]Search [3]Insert \n[4]Delete [5]Store in ASCII\n[6]Store in binary\n[7]QUIT\n#####################\n");
        scanf("%d", &a);
        
        if (a == 1){
            int flag = 0;
            printf("찾을 단어를 입력하세요: ");
            scanf("%s", word);
            word[0] = toupper(word[0]);
            int index = word[0] - 'A';
            
            NODE *temp1 = list[index].header;
            while (temp1 != list[index].trailer){
                
                if (!strcmp(temp1->element, word)){
                    flag++;
                    addLast(&voc, temp1->element);
                }
                temp1 = temp1->next;
            }
            
            if (flag != 0){
                printf("사전에 있습니다. \n");
            }
            else
                printf("사전에 없습니다. \n");
        }
        
        else if (a == 2){
            
            int flag = 0;
            printf("찾을 단어를 입력하세요: ");
            scanf("%s", word);
            word[0] = toupper(word[0]);
            int index = word[0] - 'A';
            
            NODE *temp1 = voc.header;
            while (temp1 != voc.trailer){
                
                if (!strcmp(temp1->element, word)){
                    flag++;
                }
                temp1 = temp1->next;
            }
            
            if (flag != 0)
                printf("단어장에 있습니다. \n");
            else
                printf("단어장에 없습니다. \n");
        }
        
        else if (a == 3){
            
            printf("삽입할 단어를 입력하세요: ");
            scanf("%s", word);
            word[0] = toupper(word[0]);
            addLast(&voc, word);
            printf("%s가 삽입되었습니다.\n ", word);
            
        }
        
        
        else if (a == 4){
            printf("삭제할 단어를 입력하세요: ");
            scanf("%s", word);
            word[0] = toupper(word[0]);
            int index = word[0] - 65;
            int flag = 0;
            
            NODE *temp1 = voc.header;
            while (temp1 != voc.trailer){
                if (!strcmp(temp1->element, word)){
                    temp1->next->prev = temp1->prev;
                    temp1->prev->next = temp1->next;
                    flag++;
                }
                temp1 = temp1->next;
            }
            if (flag != 0)
                printf("삭제하였습니다. \n");
            else
                printf("단어장에 없습니다. \n");
        }
        
        else if (a == 5){
            FILE *fp = fopen("abc.txt", "w");
            NODE *temp = voc.header;
            while (temp != voc.trailer){
                fprintf(fp, "%s  ", temp->element);
                temp = temp->next;
            }
            printf("abc.txt에 저장되었습니다. \n");
            fclose(fp);
        }
        else if (a == 6){
            FILE *fp = fopen("abcd.bin", "wb");
            NODE *temp = voc.header;
            while (temp != voc.trailer){
                fwrite(temp->element, sizeof(temp->element), 1, fp);
                
                temp = temp->next;
            }
            printf("abcd.bin에 저장되었습니다. \n");
            fclose(fp);
        }
    }
}

