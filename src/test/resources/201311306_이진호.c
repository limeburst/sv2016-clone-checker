#include<stdio.h>
#include<ctype.h>
#include<string.h>
#define TRUE 1
#define FALSE 0


typedef struct data{
   char word[256];

}LData;

typedef struct _node{
   LData data;
   struct _node * prev;
   struct _node * next;
}Node;

typedef struct _linkedlist{
   Node * head;
   Node * cur;
   Node * trailer;
   int numOfData;

}LinkedList;

typedef LinkedList list;

void ListInit(LinkedList * plist);
void LInsert(LinkedList * plist,LData data);
int LFirst(LinkedList * plist,LData * pdata);
int LNext(LinkedList * plist, LData * pdata);
int LPrevious(LinkedList * plist,LData * pdata);
LData LRemove(LinkedList * plist);
int LRemoveData(LinkedList* plist,LData data);
int LSearch(LinkedList* plist,LData data);
int LCount(LinkedList * plist);



int main(){
   int i,sel,c;
   list list[26];
   FILE* fp;  //텍스트 파일 읽어올떄 쓰는 파일포인터
   FILE* wfp1;  //ASCII로 저장
   FILE* wfp2;  //binary로 저장
   char buffer[256];  //텍스트 파일에서 읽어온 단어 저장
   char word[256];  //입력받은 단어
   LData in;  //리스트에 저장할떄 임시로 사용
   char* str; //토큰으로 나눈단어 임시 저장.
   char sep[]=" ";   

   if((fp=fopen("40-Matthew.txt","r")) == NULL)
      printf("파일열기 실패\n");


   for(i=0;i<26;i++){
      ListInit(&list[i]);
   }

   while(fgets(buffer,sizeof(buffer),fp)!=NULL){
      str=strtok(buffer," \r\n");
      while(str!=NULL){
         strcpy(in.word,str);
         if(isalpha(str[0])){
            c=toupper(str[0]);
            LInsert(&list[c-65],in);
         }else{
            
         }
         str=strtok(NULL," \r\n");

      }
   }

   while(1){
      printf("기능을 선택하세요.\n");
      printf("0.exit 1.search 2.insert 3.delete\n4.store in ASCII 5.store in binary\n");
      scanf("%d",&sel);

      if(sel == 0){
         break;
      }else if(sel == 1){
         printf("찾을 단어를 입력하세요.");
         scanf("%s", word);
         strcpy(in.word,word);
         if(isalpha(word[0])){
            c=toupper(word[0]);
            if(LSearch(&list[c-65],in))
               printf("찾는 단어가 있습니다.\n");
            else
               printf("찾는 단어가 없습니다.\n");

         }else
            printf("영어로 시작하는 단어가 아니네?\n");


      }else if(sel == 2){
         printf("입력할 단어를 입력하세요.");
         scanf("%s",word);
         strcpy(in.word,word);
         if(isalpha(word[0])){
            c=toupper(word[0]);
            LInsert(&list[c-65],in);
            printf("저장 되었습니다.\n"); 
         }else
            printf("영어로 시작해야 합니다.\n");

      }else if(sel == 3){
         printf("삭제할 단어를 입력하세요.");
         scanf("%s",word);
         strcpy(in.word,word);
         if(isalpha(word[0])){
            c=toupper(word[0]);
            LRemoveData(&list[c-65],in);
            printf("제거 되었습니다.\n");
         }else
            printf("영어로 시작하는 단어이어야 합니다.\n");

      }else if(sel == 4){
         printf("ASCII 로 저장합니다.\n");
         if(remove("ascii_voc.txt") == -1)
            printf("파일삭제가 안됨;\n\n");
         if((wfp1=fopen("ascii_voc.txt","w")) == NULL)
            printf("아스키 단어장 열기가 안됨.\n");

         for(i=0;i<26;i++){      
            if(LFirst(&list[i],&in)){
               strcat(in.word,sep);
               fputs(in.word,wfp1);
               while(LNext(&list[i],&in)){
                  strcat(in.word,sep);
                  fputs(in.word,wfp1);
               }
            }
            fflush(wfp1);
         }


      }else if(sel == 5){
         printf("binary로 저장합니다.\n");
         if(remove("binary_voc.txt") == -1)
            printf("파일 삭제가 안됨.\n");
         if((wfp2=fopen("binary_voc.txt","wb")) == NULL)
            printf("바이너리 단어장 열기 실패.\n");

         for(i=0;i<26;i++){
            if(LFirst(&list[i],&in)){
               //fputs(in.word,wfp2);
               fwrite(in.word,sizeof(char),strlen(in.word),wfp2);
               while(LNext(&list[i],&in)){
                  //fputs(in.word,wfp2);
                  fwrite(in.word,sizeof(char),strlen(in.word),wfp2);
               }
            }
            fflush(wfp2); 

         }


      }else{
         printf("다시 입력해주세요.\n");
      }

   }

   fclose(fp);
   fclose(wfp1);
   fclose(wfp2);

   return 0;
   
}
void ListInit(LinkedList* plist){
   plist->head = (Node*)malloc(sizeof(Node));
   plist->trailer = (Node*)malloc(sizeof(Node));

   plist->head->prev = NULL;
   plist->head->next = plist->trailer;

   plist->trailer->next = NULL;
   plist->trailer->prev = plist->head;

   plist->numOfData = 0;
}
void LInsert(LinkedList* plist,LData data){
   Node* newNode = (Node*)malloc(sizeof(Node));
   newNode->data = data;

   newNode->prev = plist->trailer->prev;
   plist->trailer->prev->next = newNode;

   newNode->next = plist->trailer;
   plist->trailer->prev = newNode;

   (plist->numOfData)++;
}
int LFirst(LinkedList* plist,LData* pdata){
   if(plist->head->next == plist->trailer)
      return FALSE;

   plist->cur = plist->head->next;
   *pdata = plist->cur->data;

   return TRUE;
}
int LNext(LinkedList* plist, LData* pdata){
   if(plist->cur->next == plist->trailer)
      return FALSE;

   plist->cur = plist->cur->next;
   *pdata = plist->cur->data;

   return TRUE;
}
int LPrevious(LinkedList* plist,LData* pdata){
   if(plist->cur->prev == NULL)
      return FALSE;

   plist->cur = plist->cur->prev;
   *pdata = plist->cur->data;

   return TRUE;
}
LData LRemove(LinkedList* plist){
   Node* rpos = plist->cur;
   LData remv = rpos->data;

   plist->cur->prev->next = plist->cur->next;
   plist->cur->next->prev = plist->cur->prev;

   plist->cur = plist->cur->prev;

   free(rpos);
   (plist->numOfData)--;

   return remv;

}
int LRemoveData(LinkedList* plist,LData data){
   Node* rpos;
   int num=0;

   if(plist->head->next == plist->trailer)
      return FALSE;

   plist->cur = plist->head->next;

   while(plist->cur != plist->trailer){
      if(strcmp((plist->cur->data).word,data.word) == 0){
         rpos = plist->cur;
         plist->cur->prev->next = plist->cur->next;
         plist->cur->next->prev = plist->cur->prev;

         plist->cur = plist->cur->prev;

         free(rpos);
         (plist->numOfData)--;
         num++;
         
      }
      plist->cur = plist->cur->next;

   }
   if(num>0)
      return num;
   else
      return FALSE;

}
int LSearch(LinkedList* plist,LData data){
   if(plist->head->next == plist->trailer)
      return FALSE;

   plist->cur = plist->head->next;
   while(plist->cur != plist->trailer){
      
      if(strcmp((plist->cur->data).word,data.word) == 0){
         return TRUE;
      }
      plist->cur = plist->cur->next;
   }

   return FALSE;
   
}
int LCount(LinkedList* plist){
   return plist->numOfData;
}
