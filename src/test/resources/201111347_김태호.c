#include<stdio.h>
#include<string.h>
#include<windows.h>

#define MAX_COUNT 5
#pragma warning(disable:4996)



typedef struct node {
	char head;
	char word[25];
	struct node* nNext;
	struct node* nPrev;
}Node;

void insertNode(Node* head, Node* newNode) {
	Node* tempNode;
	if (head->nNext == NULL) {
		head->nNext = newNode;
		newNode->nPrev = head;
	}
	else {
		tempNode = head->nNext;
		head->nNext = newNode;
		newNode->nNext = tempNode;
		tempNode->nPrev = newNode;
		newNode->nPrev = head;
	}
}

Node* addNode(char word[], char head) {
	Node* newNode;
	newNode = (Node*)malloc(sizeof(Node));
	newNode->head = head;
	strcpy(newNode->word, word);
	newNode->nNext = NULL;
	newNode->nPrev = NULL;
}

void deleteNode(Node *target) {
	Node* tempNode;
	tempNode = target->nPrev;
	if (target->nNext == NULL) {
		tempNode->nNext = NULL;
	}
	else {
		tempNode->nNext = target->nNext;
		tempNode = target->nNext;
		tempNode->nPrev = target->nPrev;
	}
}

int getLength(Node *head) {
	Node *p;
	int length = 0;

	p = head;
	while (p) {
		length++;
		p = p->nNext;
	}
	return length;
}

Node* getNext(Node *node) {
	return node->nNext;
}

void printList(Node *head) {
	Node *p;
	printf(" %c", toupper(head->head));
	p = head;
	while (p) {
		printf("%s > ", p->word);
		p = p->nNext;
	}
	printf("\n");
}

void printListCount(Node *head, int num) {
	Node *p;
	int i;
	printf(" %c", toupper(head->head));
	p = head;
	for (i = 0; i <= num; i++) {
		printf("%s > ", p->word);
		
		p = p->nNext;
		if (p == NULL) {
			break;
		}
	}
	printf("\n");
}


void main() {
	FILE* f;
	FILE* f2;
	FILE* f3;
	char* fileName = "";
	char tempword[25];
	char buffer[2] = { NULL,NULL };
	Node *head[27]; // a~z + theOthers(+)
	Node* node;
	int i = 0;
	int function = 1;
	int index = 0;
	char word[25];



	fileName = "Sample.txt";
	f = fopen(fileName, "r");
	

	if (f == NULL) {
		printf("Error:File Not Found\n");
	}
	else {

		for (i = 0; i < 26; i++) {
			head[i] = addNode(" ", 'a' + i);
		}
		head[26] = addNode("", '+');
		
		for (i = 0; i < 25; i++) {
			tempword[i] = NULL;
		}

		while (fgets(buffer, 2, f)) {
			buffer[0] = tolower(buffer[0]);

			if (strstr(buffer, " ")) {
				if (strlen(tempword) != 0) {
					node = addNode(tempword, tempword[0]);
					index = ((int)tempword[0] - 97);
					insertNode(head[index], node);
				}
				for (i = 0; i < 25; i++) {
					tempword[i] = NULL;
				}
			}
			else if (strstr(buffer, "\n")) {
				if (strlen(tempword) != 0) {
					node = addNode(tempword, tempword[0]);
					index = ((int)tempword[0] - 97);
					insertNode(head[index], node);
				}
				for (i = 0; i < 25; i++) {
					tempword[i] = NULL;
				}
			}
			else if ((int)buffer[0]<97 || (int)buffer[0]>123) {
				continue;
			}
			else {
				strcat(tempword, buffer);
			}


		}
		for (i = 0; i < 26; i++) {
			printListCount(head[i],5);
		}
	}


	while (1) {
		Node* tempNode;
		int j = 0;

		printf("Enter Function\n");
		printf("[1]Search-List\n[2]Insert-List\n");
		printf("[3]Delete-List\n[4]Quit\n");
		printf("[5]show-List\n");
		printf("[6]show-voc.txt\n");
		printf("[7]Search-voc.txt\n");
		printf("[8]Delete-voc.txt\n");

		scanf("%d", &function);
		printf("function:%d\n", function);
	
		if (function == 1) {
			printf("Enter word : ");
			scanf("%s", &word);
			if ((int)word[0]<97 || (int)word[0]>123) {
				printf("ERR\n");
				break;
			}
			for (i = 0; i < strlen(word); i++) {
				word[i] = tolower(word[i]);
			}
			index = (int)(word[0]) - 97;
			tempNode = getNext(head[index]);

			if (strlen(word) > 25) {
				printf("ERR\n");
				function = 0;
			}

			if (tempNode == NULL) {
				printf("not found\n");
				function = 0;
			}

			while (function == 1) {
				if (strcmp(tempNode->word, word) == 0) {
					printf("search : %s\n", tempNode->word);
					printf("1.Store in binary\n");
					printf("2.Store in ASCII\n");
					scanf("%d", &function);

					if (function == 1) {
						f2 = fopen("voc.txt", "ab");
						fprintf(f2,"%s\n", tempNode->word);
						fclose(f2);
					}
					else if (function == 2) {
						f2 = fopen("voc.txt", "a");
						fprintf(f2,"%s\n", tempNode->word);
						fclose(f2);
					}
					else {

					}
					
					break;
				}
				if (tempNode->nNext == NULL) {
						printf("not found\n");
						break;
				}
				tempNode = getNext(tempNode);

			}
		}
		else if (function == 2) {
			printf("Enter word : ");
			scanf("%s", &word);
			if ((int)word[0]<97 || (int)word[0]>123) {
				printf("ERR\n");
				break;
			}
			for (i = 0; i < strlen(word); i++) {
				word[i] = tolower(word[i]);
			}
			if (strlen(word) > 25) {
				printf("ERR\n");
				function = 0;
			}

			index = (int)(word[0]) - 97;

				tempNode = addNode(word, word[0]);
				insertNode(head[index], tempNode);
		} //Insert
		else if (function == 3) {
			printf("Enter word : ");
			scanf("%s", &word);
			if ((int)word[0]<97 || (int)word[0]>123) {
				printf("ERR\n");
				break;
			}

			for (i = 0; i < strlen(word); i++) {
				word[i] = tolower(word[i]);
			}
			index = (int)(word[0]) - 97;
			tempNode = getNext(head[index]);

			if (strlen(word) > 25) {
				printf("ERR\n");
				function = 0;
			}

			if (tempNode == NULL) {
				printf("not found\n");
				function = 0;
			}

			while (function == 3) {
				Node* tempNode2;

				if (strcmp(tempNode->word, word) == 0) {
					deleteNode(tempNode);
					break;
				}
				else {
					tempNode = getNext(tempNode);
				}
			}
		} //Delete
		else if (function == 4) {
			printf("Exit\n");
			break;
		} //Exit
		else if (function == 5) {
			printf("------------Sample.txt-------------\n");
			for (i = 0; i < 26; i++) {
				printListCount(head[i],5);
			}
		} //Show.txt

		else if (function == 6){ 
			printf("------------voc.txt---------------\n");
			f2 = fopen("voc.txt", "rb");
			while (fgets(buffer, 2, f2)) {
				printf("%s", buffer);
			}
			printf("\n");
			fclose(f2);
		}//show-voc.txt

		else if (function == 7) {
			char word[25];
			char userword[25];
			int i;

			printf("Enter word to search\n");
			scanf("%s", &userword);

			for (i = 0; i < 25; i++) {
				word[i] = NULL;
			}

			f2 = fopen("voc.txt", "rb");
			while (fgets(buffer, 2, f2)) {
				if (buffer[0] == '\n' || buffer[0] == '\r') {
					if (strcmp(word,userword)==0) {
						printf("search *%s*\n", userword);
						break;
					}
					else{
						for (i = 0; i < 25; i++) {
							word[i] = NULL;
						}
					}
				}
				else {
					strcat(word, buffer);
				}
			}
			if (word[0] == NULL) {
				printf("NOT Found\n");
			}
			function = 0;
		}//search-voc.txt

		else if (function == 8) {
			char word[25];
			char userword[25];
			int i;

			printf("Enter word to delete\n");
			scanf("%s", &userword);

			for (i = 0; i < 25; i++) {
				word[i] = NULL;
			}

			f2 = fopen("voc.txt", "rb");
			f3 = fopen("temp.txt", "w");
			while (fgets(buffer, 2, f2)) {

				if (buffer[0] == '\n' || buffer[0] == '\r') {
					if (strcmp(word, userword) == 0) { //search
						printf("delete *%s*\n", userword);
						for (i = 0; i < 25; i++) {
							word[i] = NULL;
						}
					}
					else if(strlen(word) >0){
						fprintf(f3,"%s", word);
						fprintf(f3, "\n",1);
						for (i = 0; i < 25; i++) {
							word[i] = NULL;
						}
					}
				}
				else {
					strcat(word, buffer);
				}
			}
			if (word[0] != NULL) {
				printf("NOT Found\n");
			}

			fclose(f2);
			fclose(f3);


			f3 = fopen("temp.txt", "r");
			f2 = fopen("voc.txt", "w");
			while (fgets(buffer, 2, f3)) {
				fprintf(f2,"%s", buffer);
			}
			fclose(f3);
			fclose(f2);

		}//delete-voc.txt
		else {
			printf("Error : Undefined Function");
		}


	}
}

