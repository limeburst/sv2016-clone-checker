#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

typedef struct NODE {
	char word[50];
	struct NODE *link;
}NODE;

void addLast(NODE *plist, char s[], int *num) {
	NODE *nodeNew;

        nodeNew = (NODE *)malloc(sizeof(NODE));
        strcpy(nodeNew->word,s);
        nodeNew->link = NULL;

        while(plist->link) {
                plist=plist->link;
        }

        plist->link = nodeNew;
        (*num)++;
}

void print_data(NODE *plist)
{
        NODE *p;
        p = plist;

        printf("%s : ", p->word);
        p = p->link;

        printf("( ");
        while (p)
        {
                printf("%s ", p->word);
                p = p->link;
        }
        printf(")\n");
}

int search(NODE *plist, char s[])
{
        NODE *p;

        p=plist;

        while(p)
        {
                if(strcmp(p->word, s)==0)
                {
                        return 1;
                }
                p = p->link;
        }
        return 0;
}

void insert(NODE *plist, char s[], int *num)
{
	if(search(plist, s)!=1)
	{        
		addLast(plist, s, num);
        	printf("-> 단어장에 삽입되었습니다.\n");
	}
	else if(search(plist,s)==1)
		printf("-> 이미 단어장에 존재합니다.\n");
}

int delete(NODE *plist, char s[], int *num)
{
        while(plist->link)
        {
                if(strcmp(plist->link->word, s)==0)
                {
                        NODE *temp = plist->link;
                        plist->link = plist->link->link;
                        free(temp);
                        (*num)--;
                        return 1;
                }
                plist = plist->link;
        }

        if(strcmp(plist->word, s) ==0)
	{
  		plist=NULL;
                free(plist);
                (*num)--;
                return 1;
        }

        return 0;
}

void store_in_ASCII(NODE *plist, FILE *fp)
{

	NODE *p = plist;
	p = p->link;

	while(p)
	{
		fprintf(fp, "%s", p->word);
		fprintf(fp, "%s", " ");
		p = p->link;
	}
}

void store_in_binary(NODE *plist, FILE *bp)
{
	NODE *p = plist;
	p = p->link;
	char s[5] = " ";
	
	while(p)
	{
		fwrite(p,sizeof(p),1,bp);
		fwrite(s ,strlen(s), 1, bp);
		p=p->link;
	}
}

void read_ASCII() {

	FILE *fp = fopen("voc.txt", "r");
	
	char c=0;

	if(fp==NULL)
		printf("텍스트 파일을 열 수 없습니다.\n");
	else
	{
		do
		{
			c = fgetc(fp);
			if(c==20)
				printf(" ");
			else
				printf("%c", c);

		}while(!feof(fp));
	}

	fclose(fp);
		
}

void read_binary() {

	FILE *bp = fopen("voc.dat", "rb");	

	char c=0;

	if(bp==NULL)
                printf("이진 파일을 열 수 없습니다.\n");

	else
	{
		do
        	{
         		fread(&c, sizeof(char), 1, bp);
                	if(c == 20)
                		printf(" ");
			else
				printf("%c", c);

        	}while(!feof(bp));
	}

	fclose(bp);
}

int main(void) {
	char line[500];
        char seps[] = " ,.\t\n";
        char *token;
        char f_word[20];
        int menu=0, v_menu=0, i=0, index=0, d_num[26], v_num[26], low_check=-1;

	//사전 list 제작
        for(i=0;i<26;i++)
                d_num[i]=0;

        NODE *dic[26];

        for(i=0;i<26;i++)
        {
                dic[i] = (NODE *)malloc(sizeof(NODE));
                dic[i]->word[0] = i+97;
                dic[i]->word[1] = 0;
                dic[i]->link = NULL;
        }

        FILE *fp = fopen("sample.txt", "r"); 

        while(fgets(line, sizeof(line), fp))
        {
                token=strtok(line, seps);

                while(token!=NULL)
                {
                        if(isalpha(token[0]))
                        {
                                if(isupper(token[0]))
                                {
                                        token[0] = tolower(token[0]);
                                        low_check = 1;
                                }

                                index = token[0]-97;
				
				if(low_check==1)
                                        token[0] = toupper(token[0]);

                                addLast(dic[index], token, &d_num[index]);

                                low_check=-1;
                        }

                        token=strtok(NULL,seps);
                }
        }

        for(i=0;i<26;i++)
		print_data(dic[i]);

	fclose(fp); //사전 list 완료
	
	//단어장 list 제작
	NODE *voc[26];
	
	for(i=0;i<26;i++)
		v_num[i]=0;

	for(i=0;i<26;i++)
	{
		voc[i] = (NODE *)malloc(sizeof(NODE));
		voc[i]->word[0] = i+97;
		voc[i]->word[1] = 0;
		voc[i]->link = NULL;
	}
		
	//단어장 list완료

	while(menu!=3)
	{
		printf("기능을 선택하세요\n[1] Dictionary Search [2] Vocabulary [3] Quit\n: ");
		scanf("%d", &menu);
		
		if(menu==1)
		{
			printf("사전에서 찾을 단어를 입력하세요 : ");
			scanf("%s", f_word);
			if(isalpha(f_word[0]))
			{
				if(isupper(f_word[0]))
				{
                                        f_word[0] = tolower(f_word[0]);
                                        low_check=1;
                                }

                                index = f_word[0] -97;

                                if(low_check ==1)
                                        f_word[0] = toupper(f_word[0]);

                                if(search(dic[index], f_word)==1)
                                        printf("-> 사전에 있습니다.\n");
                                else
                                        printf("-> 사전에 없습니다.\n");

				insert(voc[index], f_word, &v_num[index]);
			}
                        else
                                printf("-> 잘못 입력하셨습니다.\n");
                }
		
		else if(menu==2)
		{
			printf("기능을 선택하세요 \n[1] Search [2] Insert\n[3] Delete [4] Store In Binary\n[5] Store In ASCII [6] Quit\n: ");
			scanf("%d", &v_menu);

			if(v_menu==1)
               		{
                        	printf("단어장에서 찾을 단어를 입력하세요 : ");
                        	scanf("%s", f_word);
                       		if(isalpha(f_word[0]))
                        	{
                                	if(isupper(f_word[0]))
                                	{
                                        	f_word[0] = tolower(f_word[0]);
                                       		low_check=1;
                                	}

                                	index = f_word[0] -97;

                              	  	if(low_check ==1)
                                        	f_word[0] = toupper(f_word[0]);

 					if(search(voc[index], f_word)==1)
						printf("-> 단어장에 있습니다.\n");
					else                               	
                                        	printf("-> 단어장에 없습니다.\n");
					
					for(i=0;i<26;i++)
						print_data(voc[i]);

                        	}
                        	else
                                printf("-> 잘못 입력하셨습니다.\n");
                	}
			
			else if(v_menu==2)
			{
				printf("단어장에 삽입할 단어를 입력하세요 : ");
	                        scanf("%s", f_word);
        	                if(isalpha(f_word[0]))
                	        {
                        	        if(isupper(f_word[0]))
                                	{
                                	        f_word[0] = tolower(f_word[0]);
                                       	 	low_check=1;
 	                               }

        	                        index = f_word[0] -97;

                        	        if(low_check ==1)
                                	        f_word[0] = toupper(f_word[0]);

					if(search(voc[index], f_word)==1)
						printf("-> 이미 단어장에 있습니다.\n");

					else
                  	              		insert(voc[index], f_word, &v_num[index]);
				
					for(i=0;i<26;i++)
						print_data(voc[i]);
                        	}
                        	else
                                	printf("-> 잘못 입력하셨습니다.\n");
			}
			else if(v_menu==3)
			{
				printf("단어장에서 삭제할 단어를 입력하세요 : ");
  	                	scanf("%s", f_word);
        	                if(isalpha(f_word[0]))
                	       	{
                                	if(isupper(f_word[0]))
                                	{
                                        	f_word[0] = tolower(f_word[0]);
                                        	low_check=1;
                                	}

                           		index = f_word[0] -97;

                     	        	if(low_check ==1)
                        	                f_word[0] = toupper(f_word[0]);

                                       	if(delete(voc[index], f_word, &v_num[index])==1)
                                       	        printf("-> 단어장에서 삭제되었습니다.\n");
                                       	else
                                                printf("-> 단어장에 없습니다.\n");
						
					for(i=0;i<26;i++)
						print_data(voc[i]);
                        	}
                        	else
                                	printf("-> 잘못 입력하셨습니다.\n");
                	}

			else if(v_menu==4)
			{
				printf("\n");

				FILE *bp = fopen("voc.dat", "wb");
	
				for(i=0;i<26;i++)
					store_in_binary(voc[i], bp);

				fclose(bp);
				
				read_binary();

				printf("\n");

				printf("-> 저장했습니다\n");

			}
			else if(v_menu==5)
			{
				printf("\n");

				FILE *fp = fopen("voc.txt", "w");

				for(i=0;i<26;i++)
					store_in_ASCII(voc[i], fp);

				fclose(fp);

				read_ASCII();
	
				printf("\n");

				printf("-> 저장했습니다.\n");

			}
                	else if(v_menu==6)
                        	printf("-> 종료되었습니다.\n");
			
			else
				printf("-> ERROR!\n");

                }

		else if(menu==3) {
			printf("-> 종료되었습니다.\n");
			break;
		}

		else {
			printf("-> ERROR!\n");
			break;
		}

		menu=0;
		v_menu=0;
		low_check=-1;
	}		


        for(i=0;i<26;i++) {
                free(dic[i]);
		free(voc[i]);
	}

        return 0;
}

