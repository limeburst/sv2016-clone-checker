#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <stdbool.h>

struct list_node{
    char title[30];
    struct list_node *next;
};

struct alphabet_node{
    char title[10];
    struct list_node start_node;
};

void make_voc_string(char voc_string[], struct alphabet_node* alphabet_array)
{
    // 현재 단어장에 있는 단어들을 string 으로 생성
    int i;
    char buffer[1000];
    for(i=0; i<26; i++)
    {
        struct list_node* current = NULL;
        current = &(alphabet_array[i].start_node);
        
        while( current->next != NULL)
        {
            strcat(buffer, " ");
            strcat(buffer, current->title);
            current = (*current).next;
        }
    }
    
    strcpy(voc_string, buffer);
    
    return;
}

void print_nodes(int index, struct alphabet_node array[])
{
    // 사전에 들어가있는 단어를 인덱스로 접근해서 출력
    struct list_node* current = NULL;
    current = &(array[index].start_node);
    
    while( current->next != NULL)
    {
        printf("    - %s \n", current->title);
        current = (*current).next;
    }
    
    return;
}


void print_all_nodes(struct alphabet_node array[])
{
    // 사전에 들어가있는 단어를 알파벳별로 모두 출력
    int i;
    for(i=0; i<26; i++)
    {
        printf("[index %d]\n", i);
        
        print_nodes(i, array);
    }
    
    return;
}

void insert_word(char buffer[], struct alphabet_node array[])
{
    char first_char;
    struct list_node* current;
    struct list_node* new_node;
    int node_count = 0;
    int array_index;
    
    // 새로운 노드를 생성하기 위해 메모리 할당
    new_node = (struct list_node*)malloc(sizeof(struct list_node));
    new_node->next = NULL;
    first_char = tolower(buffer[0]); // 대문자에서 소문자로 변환
    
    array_index = (int)first_char - (int)'a';
    
    // 알파벳이 아닐 경우 return
    if(array_index > 25 || array_index < 0)
    {
        return;
    }
    
    current = &(array[array_index].start_node);
    // 첫 번째 노드부터 차례로 순회
    while( current->next != NULL)
    {
        // 이미 동일한 단어가 존재할 때
        if( strcmp(current->title, buffer) == 0 )
            return;
        node_count++;
        current = ((*current).next);
    }
    
    node_count++;
    
    current->next = new_node;
    strcpy(current->title, buffer);
    return;
}

struct list_node* search_word(char*buffer, struct alphabet_node* array)
{
    // 사전에 링크되어있는 모든 노드들 중에 검색된 노드를 리턴
    char first_char;
    int array_index;
    struct list_node* current = NULL;
    
    first_char = tolower(buffer[0]); // 대문자를 소문자로 변환
    array_index = (int)first_char - (int)'a';
    
    if (array_index > 25 || array_index <0 )
    {
        // 만약 알파벳이 아니라면 NULL return
        return NULL;
    }
    
    current = &(array[array_index].start_node);
    
    // 첫번째 노드부터 차례대로 방문
    while( strcmp(current->title, buffer) != 0 && current->next != NULL)
    {
        current = (*current).next;
    }
    
    if ( strcmp(current->title, buffer) == 0)
    {
        // 검색어가 일치할 경우
        return current;
    }else{
        // 찾지 못했을 경우 NULL return
        return NULL;
    }
}

bool delete_word(char buffer[], struct alphabet_node array[])
{
    char first_char;
    int array_index;
    struct list_node* current = NULL;
    struct list_node* prev = NULL;
    
    first_char = tolower(buffer[0]);
    array_index = (int)first_char - (int)'a';
    if(array_index > 25 || array_index < 0)
    {
        return false;
    }
    
    current = &(array[array_index].start_node);
    
    // 삭제할 노드와 그 노드 바로 앞의 노드를 가리키는 포인터 세팅
    while(strcmp(current->title, buffer) != 0 && current->next != NULL)
    {
        prev = current;
        current = (*current).next;
    }
    
    if(strcmp(current->title, buffer) == 0)
    {
        // 만약 해댱하는 노드가 있을 경우
        if(prev != NULL)
        {
            // 삭제 노드 이전 노드의 다음 노드를 다시 설정
            // 삭제할 노드가 첫 번째 노드가 아닌 경우
            prev->next = current->next;
            free(current);
        }else{
            // 삭제할 노드가 첫 번째 노드인 경우
            array[array_index].start_node = *(current->next);
        }
        return true;
    }else{
        return false;
    }
}

void init_dict(char* file_name, struct alphabet_node* alphabet_array)
{
    // 처음 텍스트 파일을 읽고 배열을 초기화한다.
    FILE* ptr_file;
    char buf[1000];
    char seps[] = " \t\n,.\"\'\?";
    char* token;
    
    ptr_file = fopen(file_name, "r");
    if(!ptr_file)
        return;
    
    while(fgets(buf, 1000, ptr_file) != NULL)
    {
        token = strtok(buf, seps);
        while(token != NULL)
        {
            insert_word(token, alphabet_array);
            token = strtok(NULL, seps);
        }
    }
    fclose(ptr_file);
    return;
}

void init_voc(char* file_name, struct alphabet_node* alphabet_array)
{
    // 처음 텍스트 파일을 읽고 배열을 초기화한다.
    FILE* ptr_file;
    char buf[1000];
    char seps[] = " \t\n,.\"\'\?";
    char* token;
    
    ptr_file = fopen(file_name, "rb");
    if(!ptr_file)
        return;
    
    while(fread(&buf, sizeof(char), 1000, ptr_file))
    {
        token = strtok(buf, seps);
        while(token != NULL)
        {
            insert_word(token, alphabet_array);
            token = strtok(NULL, seps);
        }
    }
    fclose(ptr_file);
    return;
}


void store_in_ascii(char buffer[], struct alphabet_node* alphabet_array)
{
    FILE* ptr_file;
    ptr_file = fopen("voc.txt", "w");
    if(!ptr_file)
        return;
    
    fprintf(ptr_file, "%s", buffer);
    return;
    
}

void store_in_binary(char buffer[], struct alphabet_node* alphabet_array)
{
    FILE* ptr_file;
    ptr_file = fopen("voc.txt", "wb");
    if(!ptr_file)
        return;
    
    fwrite(buffer, sizeof(char), strlen(buffer), ptr_file);
    return;
}

int main(void)
{
    int selected_menu=0;
    int inner_selected_menu=0;
    char dict_file_name[30] = "sample.txt";
    char voc_file_name[30] = "voc.txt";
    char buffer[30];
    char voc_string[1000];
    char typed_word[30];
    int i;
    bool result;
    
    struct alphabet_node dict_alphabet_array[26] = {
        {"a"},
        {"b"},
        {"c"},
        {"d"},
        {"e"},
        {"f"},
        {"g"},
        {"h"},
        {"i"},
        {"j"},
        {"k"},
        {"l"},
        {"m"},
        {"n"},
        {"o"},
        {"p"},
        {"q"},
        {"r"},
        {"s"},
        {"t"},
        {"u"},
        {"v"},
        {"w"},
        {"x"},
        {"y"},
        {"z"}
    };
    
    struct alphabet_node voc_alphabet_array[26] = {
        {"a"},
        {"b"},
        {"c"},
        {"d"},
        {"e"},
        {"f"},
        {"g"},
        {"h"},
        {"i"},
        {"j"},
        {"k"},
        {"l"},
        {"m"},
        {"n"},
        {"o"},
        {"p"},
        {"q"},
        {"r"},
        {"s"},
        {"t"},
        {"u"},
        {"v"},
        {"w"},
        {"x"},
        {"y"},
        {"z"}
    };
    
    // 알파벳 구조체들의 첫 번째 노드가 갖는 포인터를 초기화
    for(i=0; i<26; i++)
    {
        dict_alphabet_array[i].start_node.next = NULL;
        voc_alphabet_array[i].start_node.next = NULL;
    }
    
    // 텍스트에 있는 단어를 사전에 추가
    init_dict(dict_file_name, dict_alphabet_array);
    init_voc(voc_file_name, voc_alphabet_array);
    
    // 메뉴 선택
    while(selected_menu != -1)
    {
        printf("\n기능을 선택하세요 \n");
        printf("\n [1] 사전 단어 검색 [2] 단어장 관리 [3] 종료 \n");
        printf(": ");
        
        scanf("%d", &selected_menu);
        
        if(selected_menu == 1)
        {
            printf("사전에서 찾을 단어를 입력하세요 : ");
            scanf("%s", buffer);
            if (search_word(buffer, dict_alphabet_array) != NULL)
            {
                printf("사전에 있습니다. \n");
                insert_word(buffer, voc_alphabet_array);
                printf("단어장에 추가되었습니다. \n");
            } else{
                printf("사전에 없습니다. \n");
            }
        }else if(selected_menu == 2)
        {
            inner_selected_menu = 0;
            while(inner_selected_menu != -1)
            {
                printf("\n기능을 선택하세요 \n");
                printf("[1] Search [2] Insert\n");
                printf("[3] Delete [4] Print \n");
                printf("[5] store in ASCII [6] store in binary\n");
                printf("[7] 돌아가기\n");
                printf(": ");
                
                scanf("%d", &inner_selected_menu);
                
                if(inner_selected_menu == 1)
                {
                    printf("찾을 단어를 입력하세요 : ");
                    scanf("%s", buffer);
                    if (search_word(buffer, voc_alphabet_array) != NULL)
                        printf("단어장에 있습니다. \n");
                    else
                        printf("단어장에 없습니다. \n");
                }else if(inner_selected_menu == 2)
                {
                    printf("삽입할 단어를 입력하세요 : ");
                    scanf("%s", buffer);
                    insert_word(buffer, voc_alphabet_array);
                    printf("추가되었습니다. \n");
                }else if(inner_selected_menu == 3)
                {
                    printf("삭제할 단어를 입력하세요 : ");
                    scanf("%s", buffer);
                    result = delete_word(buffer, voc_alphabet_array);
                    if (result)
                        printf("삭제되었습니다. \n");
                    else
                        printf("삭제에 실패했습니다. \n");
                }else if(inner_selected_menu == 4)
                {
                    print_all_nodes(voc_alphabet_array);
                }else if(inner_selected_menu == 5){
                    make_voc_string(voc_string, voc_alphabet_array);
                    store_in_ascii(voc_string, voc_alphabet_array);
                }else if(inner_selected_menu == 6){
                    make_voc_string(voc_string, voc_alphabet_array);
                    store_in_binary(voc_string, voc_alphabet_array);
                }else{
                    printf("돌아갑니다... \n");
                    printf("\n");
                    inner_selected_menu = -1;
                }
            }
        }else{
            printf("종료합니다. \n");
            printf("\n");
            selected_menu = -1;
        }
    }
    return 0;
}